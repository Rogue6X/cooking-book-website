﻿using CloudinaryDotNet.Actions;
using Microsoft.AspNetCore.Http;

namespace CookingWebApi.BLL.Abstractions;

public interface IPhotoService
{
    Task<ImageUploadResult> AddPhotoAsync(IFormFile? file);
    Task<DeletionResult> DeletePhotoAsync(string publicId);
}