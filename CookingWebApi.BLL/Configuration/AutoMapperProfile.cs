﻿using AutoMapper;
using CookingWebApi.BLL.Contracts.Category;
using CookingWebApi.BLL.Contracts.Ingredients;
using CookingWebApi.BLL.Contracts.Products;
using CookingWebApi.BLL.Contracts.Recipe;
using CookingWebApi.BLL.Contracts.Reviews;
using CookingWebApi.BLL.Contracts.Steps;
using CookingWebApi.BLL.Models;
using CookingWebApi.DAL.Entities;

namespace CookingWebApi.BLL.Configuration;

/// <summary>
/// The Automapper profile
/// </summary>
public class AutoMapperProfile : Profile
{
    public AutoMapperProfile()
    {
        CreateMap<Recipe, RecipeModel>().ReverseMap();
        CreateMap<Recipe, AddRecipeRequest>().ReverseMap();
        CreateMap<Recipe, GetRecipeFullDetailsResponse>().ReverseMap();
        CreateMap<Recipe, GetRecipePagedResponse>().ReverseMap();
        CreateMap<Ingredient, IngredientModel>().ReverseMap();
        CreateMap<Ingredient, IngredientBody>().ReverseMap();
        CreateMap<Ingredient, IngredientUpdateBody>().ReverseMap();
        CreateMap<Step, StepModel>().ReverseMap();
        CreateMap<Step, StepBody>().ReverseMap();
        CreateMap<Product, ProductModel>().ReverseMap();
        CreateMap<Product, ProductBody>().ReverseMap();
        CreateMap<Category, CategoryModel>().ReverseMap();
        CreateMap<Category, CategoryLightModel>().ReverseMap();
        CreateMap<Category, CategoryBody>().ReverseMap();
        CreateMap<Category, CategoryListModel>().ReverseMap();
        CreateMap<Review, ReviewModel>().ReverseMap();
        CreateMap<Review, ReviewBody>().ReverseMap();
    }
}