﻿namespace CookingWebApi.BLL.Configuration
{
    public class CloudinarySettings
    {
        public const string Path = "Cloudinary";
        public string CloudName { get; set; }
        public string Apikey { get; set; }
        public string ApiSecret { get; set; }
    }
}