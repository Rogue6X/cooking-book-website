﻿using CookingWebApi.BLL.Models;

namespace CookingWebApi.BLL.Contracts.Recipe;

public class GetRecipeFullDetailsResponse
{
    public Guid Id { get; set; }

    public string Title { get; set; }

    public Uri? OriginalLink { get; set; }
    
    public int? PrepareTime { get; set; }

    public int? CookTime { get; set; }
    
    public int? TotalTime { get; set; }

    public float? Rating { get; set; }

    public string? Description { get; set; }

    public float? Yield { get; set; }
    
    public string? PhotoUrl { get; set; }
    
    public DateTime Created { get; set; } 

    public DateTime? Updated { get; set; }

    public List<CategoryLightModel> Categories { get; set; }

    public List<IngredientModel> Ingredients { get; set; }

    public List<StepModel> Steps { get; set; }
    
    public List<ReviewModel> Reviews { get; set; }
}