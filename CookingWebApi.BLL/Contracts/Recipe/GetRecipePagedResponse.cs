﻿namespace CookingWebApi.BLL.Contracts.Recipe;

/// <summary>
/// The recipe pagination response including summary for displaying card info
/// </summary>
public class GetRecipePagedResponse
{
    public Guid Id { get; set; }

    public string Title { get; set; }

    public Uri? OriginalLink { get; set; }

    public string UrlTitle { get; set; }
    
    public int? PrepareTime { get; set; }

    public int? CookTime { get; set; }
    
    public int? TotalTime { get; set; }

    public float? Rating { get; set; }

    public string? Description { get; set; }

    public float? Yield { get; set; }
    
    public string? PhotoUrl { get; set; }
    
    public DateTime Created { get; set; }

    public DateTime? Updated { get; set; }
}