﻿namespace CookingWebApi.BLL.Contracts.Recipe;

public class AddRecipeRequest
{
    public string Title { get; set; }

    public Uri? OriginalLink { get; set; }
    
    public int? PrepareTime { get; set; }

    public int? CookTime { get; set; }

    public float? Rating { get; set; }

    public string? Description { get; set; }

    public float? Yield { get; set; }
    
    public string? PhotoUrl { get; set; }
    
    public int? TotalTime { get; set; }
}