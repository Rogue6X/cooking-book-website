﻿namespace CookingWebApi.BLL.Contracts.Steps;

public class StepUpdateBody
{
    public int StepNumber { get; set; }

    public string Description { get; set; }
    
    public List<Uri>? PhotoUrls { get; set; }
}