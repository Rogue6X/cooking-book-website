﻿namespace CookingWebApi.BLL.Contracts.Products;

/// <summary>
/// The body of the product
/// </summary>
public class ProductBody
{
    public string Name { get; set; }

    public List<string>? Substitutes { get; set; }
}