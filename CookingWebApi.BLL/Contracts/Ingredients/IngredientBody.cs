﻿namespace CookingWebApi.BLL.Contracts.Ingredients;

/// <summary>
/// The ingredient body.
/// </summary>
public class IngredientBody
{
    /// <summary>
    /// The recipe id
    /// </summary>
    public Guid RecipeId { get; set; }
    
    /// <summary>
    /// Amount of product
    /// </summary>
    public float Amount { get; set; }

    /// <summary>
    /// Unit of product
    /// </summary>
    public string Unit { get; set; }
    
    /// <summary>
    /// The notes
    /// </summary>
    public string? Notes { get; set; }

    /// <summary>
    /// The product id
    /// </summary>
    public Guid ProductId { get; set; }
}