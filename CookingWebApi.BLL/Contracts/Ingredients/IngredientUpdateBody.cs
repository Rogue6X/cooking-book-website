﻿namespace CookingWebApi.BLL.Contracts.Ingredients;

/// <summary>
/// The ingredient body.
/// </summary>
public class IngredientUpdateBody
{
    /// <summary>
    /// Amount of product
    /// </summary>
    public float Amount { get; set; }

    /// <summary>
    /// Unit of product
    /// </summary>
    public string? Unit { get; set; }

    /// <summary>
    /// The notes
    /// </summary>
    public string? Notes { get; set; }

    /// <summary>
    /// The product name
    /// </summary>
    public string ProductName { get; set; }
}