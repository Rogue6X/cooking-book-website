﻿namespace CookingWebApi.BLL.Contracts.Reviews;

public class ReviewBody
{
    public Guid RecipeId { get; set; }

    public string Author { get; set; }

    public float Rating { get; set; }
    
    public string Text { get; set; }
}