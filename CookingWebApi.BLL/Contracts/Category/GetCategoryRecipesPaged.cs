﻿using CookingWebApi.DAL.Pagination;

namespace CookingWebApi.BLL.Contracts.Category;

public class GetCategoryRecipesPaged
{
    public CategoryListModel Category { get; set; }

    public PaginationHeader Page { get; set; }
}