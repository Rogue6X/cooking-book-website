﻿using CookingWebApi.BLL.Contracts.Recipe;

namespace CookingWebApi.BLL.Contracts.Category;

public class CategoryListModel
{
    public Guid Id { get; set; }

    public string Name { get; set; }

    public string UrlName { get; set; }

    public List<GetRecipePagedResponse>? Recipes { get; set; }
}