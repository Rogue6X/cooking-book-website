﻿namespace CookingWebApi.BLL.Contracts.Category;

public class CategoryBody
{
    public string Name { get; set; }
}