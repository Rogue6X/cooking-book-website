﻿using CloudinaryDotNet;
using CloudinaryDotNet.Actions;
using CookingWebApi.BLL.Abstractions;
using CookingWebApi.BLL.Configuration;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;

namespace CookingWebApi.BLL.Facades
{
    public class PhotoService : IPhotoService
    {
        private readonly Cloudinary _cloudinary;

        public PhotoService(IOptions<CloudinarySettings> options)
        {
            var optionsValue = options.Value;
            var acc = new Account(optionsValue.CloudName, optionsValue.Apikey,
                optionsValue.ApiSecret);
            _cloudinary = new Cloudinary(acc);
        }

        public async Task<ImageUploadResult> AddPhotoAsync(IFormFile? file)
        {
            var uploadResult = new ImageUploadResult();
            if (file == null)
                return uploadResult;
            
            await using var stream = file.OpenReadStream();
            var uploadParams = new ImageUploadParams
            {
                File = new FileDescription(file.FileName, stream)
            };

            uploadResult = await _cloudinary.UploadAsync(uploadParams);

            return uploadResult;
        }

        public async Task<DeletionResult> DeletePhotoAsync(string publicId)
        {
            var deleteParams = new DeletionParams(publicId);
            
            var result = await _cloudinary.DestroyAsync(deleteParams);
            
            return result;
        }
    }
}