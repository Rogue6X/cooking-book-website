﻿using AutoMapper;
using CookingWebApi.BLL.Contracts.Products;
using CookingWebApi.BLL.Contracts.Recipe;
using CookingWebApi.BLL.Contracts.Steps;
using CookingWebApi.BLL.Models;
using CookingWebApi.BLL.Utils;
using CookingWebApi.DAL;
using CookingWebApi.DAL.Entities;

namespace CookingWebApi.BLL.Facades;

public class ProductsFacade : BaseFacade<ProductModel, Product>
{
    public ProductsFacade(UoW uoW, IMapper mapper) : base(uoW, mapper)
    {
        Repository = uoW.ProductsRepository;
    }

    public async Task<ProductModel> Add(ProductBody body)
    {
        var entity = Mapper.Map<Product>(body);
        var result = await Repository.AddAsync(entity);
        await UoW.SaveAsync();

        var model = Mapper.Map<ProductModel>(result);
        return model;
    }
    
    public async Task<List<Guid>> AddBulk(List<ProductBody> content)
    {
        var products = Mapper.Map<List<Product>>(content);

        var guids = new List<Guid>();
        foreach (var product in products)
        {
            var result = await Repository.AddAsync(product);
            guids.Add(result.Id);
        }

        await UoW.SaveAsync();

        return guids;
    }

    public async Task Update(Guid id, ProductBody body)
    {
        var entity = await GetByIdAsync(id);

        body.CopyProperties(entity);
        await UoW.SaveAsync();
    }
}