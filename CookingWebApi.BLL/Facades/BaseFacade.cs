﻿using System.Linq.Expressions;
using AutoMapper;
using CookingWebApi.BLL.Exceptions;
using CookingWebApi.BLL.Utils;
using CookingWebApi.DAL;
using CookingWebApi.DAL.Interfaces;

namespace CookingWebApi.BLL.Facades;

/// <summary>
/// The base facade
/// </summary>
/// <typeparam name="T">Model type</typeparam>
/// <typeparam name="TEntity">Entity type</typeparam>
public abstract class BaseFacade<T, TEntity>
{
    internal readonly UoW UoW;
    internal readonly IMapper Mapper;
    internal IBaseRepository<TEntity> Repository;

    /// <summary>
    /// The ctor for <see cref="BaseFacade{T,TEntity}"/>
    /// </summary>
    /// <param name="uoW">The unit of work</param>
    /// <param name="mapper">The mapper</param>
    protected BaseFacade(UoW uoW, IMapper mapper)
    {
        UoW = uoW;
        Mapper = mapper;
    }

    /// <summary>
    /// Get bulk entities from table with filtering
    /// </summary>
    public List<T> GetBulkPagedWithFilter(int limit, int offset, string filterBy, string filterValue,
        string sortBy)
    {
        var filterExpression = filterBy.CreateFilterByExpression<TEntity>(filterValue);
        var result = Repository.GetBulkPaged(limit, offset, sortBy, filterExpression);

        return Mapper.Map<List<T>>(result);
    }

    /// <summary>
    /// Get bulk entities from table
    /// </summary>
    public List<T> GetBulkPaged(int limit, int offset, string sortBy)
    {
        var result = Repository.GetBulkPaged(limit, offset, sortBy, null);

        return Mapper.Map<List<T>>(result);
    }

    /// <summary>
    /// Get bulk entities from table
    /// </summary>
    public List<T> GetBulk(string? filterBy, string? filterValue)
    {
        Expression<Func<TEntity, bool>>? filter = null;

        if (filterBy != null && filterValue != null)
            filter = filterBy.CreateFilterByExpression<TEntity>(filterValue);
        var result = Repository.GetBulk(filter);

        return Mapper.Map<List<T>>(result);
    }

    /// <summary>
    /// Get entity by id 
    /// </summary>
    /// <param name="id">The id of entity</param>
    public virtual async Task<T> GetByIdAsync(Guid id)
    {
        var result = await FindAndHandleNotFound(id);

        return Mapper.Map<T>(result);
    }

    /// <summary>
    /// Deletes entity by id
    /// </summary>
    /// <param name="id">The id of entity</param>
    public async Task DeleteByIdAsync(Guid id)
    {
        var entity = await FindAndHandleNotFound(id);

        Repository.Delete(entity);

        await UoW.SaveAsync();
    }

    private async Task<TEntity> FindAndHandleNotFound(Guid id)
    {
        var result = await Repository.GetByIdAsync(id);
        if (result == null)
        {
            throw new NotFoundCwbException($"The entity with id {id} wasn't found");
        }

        return result;
    }
    
    /// <summary>
    /// Get count of entities in table
    /// </summary>
    /// <returns></returns>
    public int GetCount()
    {
        return Repository.Count();
    }
}