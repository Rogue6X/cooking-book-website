﻿using AutoMapper;
using CookingWebApi.BLL.Contracts.Category;
using CookingWebApi.BLL.Contracts.Recipe;
using CookingWebApi.BLL.Exceptions;
using CookingWebApi.BLL.Models;
using CookingWebApi.BLL.Utils;
using CookingWebApi.DAL;
using CookingWebApi.DAL.Entities;
using CookingWebApi.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace CookingWebApi.BLL.Facades;

public class CategoryFacade : BaseFacade<CategoryModel, Category>
{
    private readonly IRecipeRepository _recipeRepository;

    public CategoryFacade(UoW uoW, IMapper mapper, IRecipeRepository recipeRepository) : base(uoW, mapper)
    {
        _recipeRepository = recipeRepository;
        Repository = uoW.CategoryRepository;
    }

    /// <summary>
    /// Get bulk entities from table
    /// </summary>
    public List<CategoryListModel> GetBulk(int limit, int offset)
    {
        var result = UoW.CategoryRepository.GetBulkPaged(limit, offset);

        return Mapper.Map<List<CategoryListModel>>(result);
    }

    public async Task<List<RecipeModel>> GetRecipesFromCategory(string name)
    {
        var category = await UoW.CategoryRepository.GetCategoryWithRecipes(name);
        if (category == null)
            throw new NotFoundCwbException($"The category with name {name} wasn't found");

        var recipeModels = Mapper.Map<List<RecipeModel>>(category.Recipes);
        return recipeModels;
    }

    public async Task<(CategoryListModel, int)> GetRecipesFromCategoryPaged(string name, int limit, int offset)
    {
        var category = await UoW.CategoryRepository.GetCategoryWithRecipes(name);
        if (category == null)
            throw new NotFoundCwbException($"The category with name {name} wasn't found");

        var recipes = category.Recipes.OrderBy(x => x.Rating).Skip(offset).Take(limit);

        var recipeModels = Mapper.Map<List<GetRecipePagedResponse>>(recipes);
        var categoryModel = Mapper.Map<CategoryListModel>(category);
        categoryModel.Recipes = recipeModels;

        var count = category.Recipes.Count;
        return (categoryModel, count);
    }

    public List<CategoryListModel> GetCategoriesWithThumbnail(int limit)
    {
        var categories = UoW.CategoryRepository.GetCategoriesWithThumbnailRecipe(limit);

        List<CategoryListModel> categoryModels = new();
        foreach (var category in categories)
        {
            var categoryModel = Mapper.Map<CategoryListModel>(category);

            categoryModels.Add(categoryModel);
        }
        return categoryModels;
    }

    public async Task<CategoryModel> Add(CategoryBody body)
    {
        var entity = Mapper.Map<Category>(body);
        var result = await Repository.AddAsync(entity);
        await UoW.SaveAsync();

        var model = Mapper.Map<CategoryModel>(result);
        return model;
    }

    public async Task<CategoryModel> AddRecipesToCategory(Guid id, List<Guid> recipeIds)
    {
        var entity = await Repository.GetByIdAsync(id);
        if (entity == null)
            throw new NotFoundCwbException("The requested category wasn't found");

        var recipes = _recipeRepository.GetBulkPaged(recipeIds.Count, 0,
            "Name", x => recipeIds.Contains(x.Id));
        entity.Recipes.AddRange(recipes);

        var model = Mapper.Map<CategoryModel>(entity);
        return model;
    }

    public async Task Update(Guid id, CategoryBody body)
    {
        var entity = await Repository.GetByIdAsync(id);

        body.CopyProperties(entity);

        await UoW.SaveAsync();
    }

    public async Task<int> GetCategoriesRecipesCount(Guid id)
    {
        var category = await Repository.GetByIdAsync(id);
        if (category == null)
            throw new NotFoundCwbException($"The category with id {id} wasn't found");

        return category.Recipes.Count;
    }
}