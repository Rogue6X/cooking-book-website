﻿using AutoMapper;
using CookingWebApi.BLL.Contracts.Recipe;
using CookingWebApi.BLL.Contracts.Steps;
using CookingWebApi.BLL.Models;
using CookingWebApi.BLL.Utils;
using CookingWebApi.DAL;
using CookingWebApi.DAL.Entities;

namespace CookingWebApi.BLL.Facades;

public class StepsFacade : BaseFacade<StepModel, Step>
{
    public StepsFacade(UoW uoW, IMapper mapper) : base(uoW, mapper)
    {
        Repository = uoW.StepsRepository;
    }

    public IEnumerable<StepModel>? GetByRecipeIdAsync(Guid id)
    {
        var entities = UoW.StepsRepository.GetByRecipeId(id);

        return Mapper.Map<IEnumerable<StepModel>>(entities);
    }

    public async Task<List<StepModel>> Add(Guid recipeId, List<StepBody> steps)
    {
        var entities = Mapper.Map<List<Step>>(steps);
        var results = new List<Step>();

        foreach (var step in entities)
        {
            step.RecipeId = recipeId;
            results.Add(await Repository.AddAsync(step));
        }

        await UoW.SaveAsync();

        var model = Mapper.Map<List<StepModel>>(results);
        return model;
    }

    public async Task Update(Guid id, StepUpdateBody body)
    {
        var entity = await GetByIdAsync(id);

        body.CopyProperties(entity);
        await UoW.SaveAsync();
    }
}