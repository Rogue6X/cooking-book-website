﻿using AutoMapper;
using CookingWebApi.BLL.Contracts.Ingredients;
using CookingWebApi.BLL.Exceptions;
using CookingWebApi.BLL.Models;
using CookingWebApi.BLL.Utils;
using CookingWebApi.DAL;
using CookingWebApi.DAL.Entities;
using CookingWebApi.DAL.Interfaces;
using Microsoft.Extensions.Logging;

namespace CookingWebApi.BLL.Facades;

/// <summary>
/// The ingredients facade
/// </summary>
public class IngredientsFacade : BaseFacade<IngredientModel, Ingredient>
{
    private readonly ILogger<IngredientsFacade> _logger;
    private readonly IProductsRepository _productRepository;

    public IngredientsFacade(IMapper mapper, UoW uoW, ILogger<IngredientsFacade> logger) : base(uoW, mapper)
    {
        _logger = logger;
        Repository = UoW.IngredientsRepository;
        _productRepository = UoW.ProductsRepository;
    }

    public IEnumerable<IngredientModel>? GetByRecipeId(Guid id)
    {
        var entities = UoW.IngredientsRepository.GetByRecipeId(id);

        return Mapper.Map<IEnumerable<IngredientModel>>(entities);
    }

    public async Task<IngredientModel> Add(IngredientBody body)
    {
        var entity = Mapper.Map<Ingredient>(body);
        var result = await Repository.AddAsync(entity);
        await UoW.SaveAsync();

        var model = Mapper.Map<IngredientModel>(result);
        return model;
    }

    public async Task<List<Guid>> AddBulk(Guid recipeId, List<IngredientUpdateBody> content)
    {
        await CheckIfRecipeExistsAsync(recipeId);

        var ingredients = new List<Ingredient>();
        for (int i = 0; i < content.Count; i++)
        {
            var product = await _productRepository.GetByNameAsync(content[i].ProductName);
            if (product == null)
                throw new ValidationCwbException($"The product with name {content[i].ProductName} doesn't exist");
            ingredients.Add(Mapper.Map<Ingredient>(content[i]));
            ingredients[i].RecipeId = recipeId;
            ingredients[i].ProductId = product.Id;
        }

        var guids = new List<Guid>();
        foreach (var ingredient in ingredients)
        {
            var result = await Repository.AddAsync(ingredient);
            guids.Add(result.Id);
        }

        await UoW.SaveAsync();

        return guids;
    }

    public async Task Update(Guid id, IngredientUpdateBody body)
    {
        var entity = await GetByIdAsync(id);

        body.CopyProperties(entity);

        await UoW.SaveAsync();
    }

    private async Task CheckIfRecipeExistsAsync(Guid id)
    {
        var result = await UoW.RecipeRepository.GetByIdAsync(id);

        if (result == null)
        {
            var message = $"The recipe with id {id} wasn't found";
            _logger.LogError(message);
            throw new ValidationCwbException(message);
        }
    }
}