﻿using AutoMapper;
using CookingWebApi.BLL.Contracts.Recipe;
using CookingWebApi.BLL.Exceptions;
using CookingWebApi.BLL.Models;
using CookingWebApi.BLL.Utils;
using CookingWebApi.DAL;
using CookingWebApi.DAL.Entities;
using CookingWebApi.DAL.Interfaces;
using CookingWebApi.DAL.Pagination;

namespace CookingWebApi.BLL.Facades;

public class RecipeFacade : BaseFacade<RecipeModel, Recipe>
{
    private readonly ICategoryRepository _categoryRepository;

    public RecipeFacade(UoW uoW, IMapper mapper, ICategoryRepository categoryRepository)
        : base(uoW, mapper)
    {
        Repository = uoW.RecipeRepository;
        _categoryRepository = categoryRepository;
    }

    /// <summary>
    /// Get bulk entities from table
    /// </summary>
    public List<GetRecipePagedResponse> GetBulkPagedWithDetails(int limit, int offset, string sortBy)
    {
        var result = Repository.GetBulkPaged(limit, offset, sortBy, null);

        return Mapper.Map<List<GetRecipePagedResponse>>(result);
    }

    public List<GetRecipePagedResponse> GetBulkPagedWithFilterAndDetails(int limit, int offset, string filterBy,
        string filterValue,
        string sortBy)
    {
        var filterExpression = filterBy.CreateFilterByExpression<Recipe>(filterValue);
        var result = Repository.GetBulkPaged(limit, offset, sortBy, filterExpression);

        return Mapper.Map<List<GetRecipePagedResponse>>(result);
    }

    public async Task<GetRecipeFullDetailsResponse> GetByTitleAsync(string title)
    {
        var recipe = await UoW.RecipeRepository.GetByNameAsync(title);
        if (recipe == null)
            throw new NotFoundCwbException($"The requested recipe with title {title} wasn't found");

        return Mapper.Map<GetRecipeFullDetailsResponse>(recipe);
    }

    public CountedList<GetRecipePagedResponse> GetMultipleByTitleMatch(string title, int limit, int offset)
    {
        var list = UoW.RecipeRepository.GetMultipleByName(title, limit, offset);

        var models = Mapper.Map<IEnumerable<GetRecipePagedResponse>>(list?.List.Distinct());

        return new CountedList<GetRecipePagedResponse>(models.ToList(), list.Count);
    }

    public async Task<GetRecipeFullDetailsResponse> AddRecipe(AddRecipeRequest body)
    {
        var recipeEntity = Mapper.Map<Recipe>(body);
        var recipe = await Repository.AddAsync(recipeEntity);

        await UoW.SaveAsync();

        var recipeModel = Mapper.Map<GetRecipeFullDetailsResponse>(recipe);
        return recipeModel;
    }

    public async Task<GetRecipeFullDetailsResponse> AddRecipeToCategories(Guid id, List<string> categoriesNames)
    {
        var recipe = await Repository.GetByIdAsync(id);
        if (recipe == null)
            throw new NotFoundCwbException($"The recipe with id {id} wasn't found");

        var categories = _categoryRepository.GetCategoriesWithRecipes(
            c => categoriesNames.Contains(c.Name)).ToList();
        if (categories == null || categories.Count != categoriesNames.Count)
            throw new NotFoundCwbException($"The requested categories weren't found");

        foreach (var category in categories)
        {
            category.Recipes?.Add(recipe);
        }

        await UoW.SaveAsync();

        return Mapper.Map<GetRecipeFullDetailsResponse>(recipe);
    }

    public async Task UpdateRecipe(Guid id, AddRecipeRequest body)
    {
        var recipe = await Repository.GetByIdAsync(id);

        body.CopyProperties(recipe);
        recipe.Updated = DateTime.UtcNow;

        await UoW.SaveAsync();
    }
}