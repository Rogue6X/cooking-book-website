﻿using AutoMapper;
using CookingWebApi.BLL.Contracts.Reviews;
using CookingWebApi.BLL.Exceptions;
using CookingWebApi.BLL.Models;
using CookingWebApi.BLL.Utils;
using CookingWebApi.DAL;
using CookingWebApi.DAL.Entities;

namespace CookingWebApi.BLL.Facades;

public class ReviewFacade : BaseFacade<ReviewModel, Review>
{
    public ReviewFacade(UoW uoW, IMapper mapper) : base(uoW, mapper)
    {
        Repository = uoW.ReviewRepository;
    }

    /// <summary>
    /// Get bulk entities from table for a recipe
    /// </summary>
    public List<ReviewModel> GetBulkPaged(Guid id, int limit, int offset, string sortBy = "Id")
    {
        var result = UoW.ReviewRepository.GetBulkPaged(id, limit, offset, sortBy);

        return Mapper.Map<List<ReviewModel>>(result);
    }

    public async Task<List<ReviewModel>> GetReviewsForRecipe(Guid id)
    {
        var recipe = await UoW.RecipeRepository.GetByIdAsync(id);
        if (recipe == null)
            throw new NotFoundCwbException($"The recipe with id {id} wasn't found");

        var recipeModels = Mapper.Map<List<ReviewModel>>(recipe.Reviews);
        return recipeModels;
    }

    public async Task<ReviewModel> Add(ReviewBody body)
    {
        var entity = Mapper.Map<Review>(body);
        var result = await Repository.AddAsync(entity);
        await UoW.SaveAsync();

        var model = Mapper.Map<ReviewModel>(result);
        return model;
    }


    public async Task Update(Guid id, ReviewBody body)
    {
        var entity = await Repository.GetByIdAsync(id);

        body.CopyProperties(entity);

        await UoW.SaveAsync();
    }

    public async Task<int> GetRecipesReviewCount(Guid id)
    {
        var recipe = await UoW.RecipeRepository.GetByIdAsync(id);
        if (recipe == null)
            throw new NotFoundCwbException($"The recipe with id {id} wasn't found");

        return recipe.Reviews.Count;
    }
}