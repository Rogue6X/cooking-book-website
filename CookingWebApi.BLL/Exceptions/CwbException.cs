﻿using System.Globalization;

namespace CookingWebApi.BLL.Exceptions;

/// <summary>
/// The base CWB exception
/// </summary>
public class CwbException : Exception
{
    public CwbException()
    {
    }

    public CwbException(string message) : base(message)
    {
    }

    public CwbException(string message, params object[] args)
        : base(String.Format(CultureInfo.CurrentCulture, message, args))
    {
    }
}