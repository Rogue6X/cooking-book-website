﻿using System.Globalization;

namespace CookingWebApi.BLL.Exceptions;

public class NotFoundCwbException : CwbException
{
    public NotFoundCwbException()
    {
    }

    public NotFoundCwbException(string message) : base(message)
    {
    }

    public NotFoundCwbException(string message, params object[] args)
        : base(String.Format(CultureInfo.CurrentCulture, message, args))
    {
    }
}