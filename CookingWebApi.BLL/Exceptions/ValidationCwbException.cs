﻿using System.Globalization;

namespace CookingWebApi.BLL.Exceptions;

/// <summary>
/// The validation exception
/// </summary>
public class ValidationCwbException : CwbException
{
    public ValidationCwbException()
    {
    }

    public ValidationCwbException(string message) : base(message)
    {
    }

    public ValidationCwbException(string message, params object[] args)
        : base(String.Format(CultureInfo.CurrentCulture, message, args))
    {
    }
}