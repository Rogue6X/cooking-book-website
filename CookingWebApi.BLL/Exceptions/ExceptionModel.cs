﻿namespace CookingWebApi.BLL.Exceptions;

/// <summary>
/// Base exception model
/// </summary>
public class ExceptionModel
{
    public string Message { get; set; }

    public int StatusCode { get; set; }
}