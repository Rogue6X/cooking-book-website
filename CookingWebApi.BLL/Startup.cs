﻿using CookingWebApi.BLL.Configuration;
using CookingWebApi.BLL.Facades;
using CookingWebApi.DAL;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace CookingWebApi.BLL;

public static class Startup
{
    private const string Appsettings = "AppSettings";

    public static void ConfigureBusinessLayerServices(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddAutoMapper(typeof(AutoMapperProfile));
        services.AddScoped<RecipeFacade>();
        services.AddScoped<ReviewFacade>();
        services.AddScoped<IngredientsFacade>();
        services.AddScoped<StepsFacade>();
        services.AddScoped<ProductsFacade>();
        services.AddScoped<CategoryFacade>();

        var appsettings = configuration.GetSection(Appsettings);
        services.AddOptions<CloudinarySettings>().Bind(appsettings.GetSection(CloudinarySettings.Path));

        services.ConfigureDataAccessLayerServices(configuration);
    }
}