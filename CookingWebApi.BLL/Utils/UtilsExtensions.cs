﻿using System.Linq.Expressions;
using System.Reflection;

namespace CookingWebApi.BLL.Utils;

internal static class UtilsExtensions
{
    /// <summary>
    ///  Extension for 'Object' that copies the properties to a destination object.
    /// </summary>
    /// <param name="source">The source.</param>
    /// <param name="destination">The destination.</param>
    public static void CopyProperties(this object source, object destination)
    {
        if (source == null || destination == null)
            throw new Exception("Source or/and Destination Objects are null");
        Type typeDest = destination.GetType();
        Type typeSrc = source.GetType();
        var results = from srcProp in typeSrc.GetProperties()
            let targetProperty = typeDest.GetProperty(srcProp.Name)
            where srcProp.CanRead
                  && targetProperty != null
                  && (targetProperty.GetSetMethod(true) != null && !targetProperty.GetSetMethod(true)!.IsPrivate)
                  && (targetProperty.GetSetMethod()!.Attributes & MethodAttributes.Static) == 0
                  && targetProperty.PropertyType.IsAssignableFrom(srcProp.PropertyType)
            select new { sourceProperty = srcProp, targetProperty = targetProperty };
        foreach (var props in results)
        {
            props.targetProperty.SetValue(destination, props.sourceProperty.GetValue(source, null), null);
        }
    }

    /// <summary>
    /// Create <see cref="Expression"/> for sorting by provided sortBy parameter
    /// </summary>
    /// <param name="sortBy">Parameter to sort by</param>
    /// <returns>Result expression</returns>
    public static Expression<Func<T, object>> CreateSortByExpression<T>(this string sortBy)
    {
        var argParam = Expression.Parameter(typeof(T), "s");
        var sortByProperty = Expression.Property(argParam, sortBy);

        return Expression.Lambda<Func<T, object>>(Expression.Convert(sortByProperty, typeof(object)), argParam);
    }

    /// <summary>
    /// Create <see cref="Expression"/> for filtering by provided filterBy field
    /// </summary>
    /// <param name="filterBy">Field to filter by</param>
    /// <param name="filterValue">Parameter to use for filtering</param>
    /// <returns>Result expression</returns>
    public static Expression<Func<T, bool>> CreateFilterByExpression<T>(this string filterBy, string filterValue)
    {
        var argParam = Expression.Parameter(typeof(T), "f");
        var filterByProperty = Expression.Property(argParam, filterBy);
        var method = typeof(string).GetMethod("Contains",new []{typeof(string)});
        var expression = Expression.Call(filterByProperty, method, Expression.Constant(filterValue));

        return Expression.Lambda<Func<T, bool>>(expression, argParam);
    }
}