﻿namespace CookingWebApi.BLL.Models;

/// <summary>
/// The category model
/// </summary>
public class CategoryModel
{
    public Guid Id { get; set; }

    public string Name { get; set; }

    public List<RecipeModel>? Recipes { get; set; }
}