﻿namespace CookingWebApi.BLL.Models;

public class ProductModel
{
    public Guid Id { get; set; }

    public string Name { get; set; }

    public List<string>? Substitutes { get; set; }
}