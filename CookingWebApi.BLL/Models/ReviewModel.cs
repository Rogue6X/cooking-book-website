﻿namespace CookingWebApi.BLL.Models;

public class ReviewModel
{
    public Guid Id { get; set; }

    public Guid RecipeId { get; set; }

    public string Author { get; set; }

    public float Rating { get; set; }
    
    public string Text { get; set; }
    
    public DateTime Created { get; set; }
}