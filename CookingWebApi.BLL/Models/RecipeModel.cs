﻿namespace CookingWebApi.BLL.Models;

/// <summary>
/// The recipe model
/// </summary>
public class RecipeModel
{
    public Guid Id { get; set; }
    
    public string Title { get; set; }

    public Uri? OriginalLink { get; set; }
    
    public int? PrepareTime { get; set; }

    public int? CookTime { get; set; }
    
    public int? TotalTime { get; set; }

    public float? Rating { get; set; }

    public string? Description { get; set; }

    public float? Yield { get; set; }
    
    public string? PhotoUrl { get; set; }
    
    public DateTime Created { get; set; } = DateTime.UtcNow;

    public DateTime? Updated { get; set; } = DateTime.UtcNow;
}