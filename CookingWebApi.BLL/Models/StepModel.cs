﻿namespace CookingWebApi.BLL.Models;

public class StepModel
{
    public Guid Id { get; set; }
    
    public Guid RecipeId { get; set; }

    public int StepNumber { get; set; }

    public string Description { get; set; }
    
    public List<Uri>? PhotoUrls { get; set; }
}