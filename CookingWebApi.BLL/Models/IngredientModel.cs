﻿using CookingWebApi.DAL.Entities;

namespace CookingWebApi.BLL.Models;

/// <summary>
/// The ingredient model.
/// </summary>
public class IngredientModel
{
    /// <summary>
    /// The Id
    /// </summary>
    public Guid Id { get; set; }

    /// <summary>
    /// The recipe id
    /// </summary>
    public Guid RecipeId { get; set; }
    
    /// <summary>
    /// Amount of product
    /// </summary>
    public float Amount { get; set; }

    /// <summary>
    /// Unit of product
    /// </summary>
    public string Unit { get; set; }
    
    /// <summary>
    /// The note
    /// </summary>
    public string? Notes { get; set; }

    /// <summary>
    /// The product id
    /// </summary>
    public Guid ProductId { get; set; }

    /// <summary>
    /// The product
    /// </summary>
    public ProductModel Product { get; set; }
}