﻿namespace CookingWebApi.BLL.Models;

/// <summary>
/// Category for returning in recipe model
/// </summary>
public class CategoryLightModel
{
    public Guid Id { get; set; }

    public string Name { get; set; }
    
    public string UrlName { get; set; }
}