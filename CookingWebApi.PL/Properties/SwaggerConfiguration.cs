﻿using System.Reflection;

namespace CookingWebApi.PL.Properties;

/// <summary>
/// Configuration methods for Swagger documentation
/// </summary>
public static class SwaggerConfiguration
{
    /// <summary>
    /// Configure swagger
    /// </summary>
    /// <param name="builder"><see cref="WebApplicationBuilder"/></param>
    public static void ConfigureSwagger(this WebApplicationBuilder builder)
    {
        builder.Services.AddSwaggerGen(options =>
        {
            var xmlFilename = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
            options.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, xmlFilename));
        });
    }
}