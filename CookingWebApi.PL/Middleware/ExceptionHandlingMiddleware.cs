﻿using System.Net;
using System.Text.Json;
using CookingWebApi.BLL.Exceptions;

namespace CookingWebApi.PL.Middleware;

/// <summary>
/// Middleware for handling errors throughout the project
/// </summary>
public class ExceptionHandlingMiddleware
{
    private readonly RequestDelegate _next;

    /// <summary>
    /// The ctor for <see cref="ExceptionHandlingMiddleware"/>
    /// </summary>
    /// <param name="next"></param>
    public ExceptionHandlingMiddleware(RequestDelegate next)
    {
        _next = next;
    }

    /// <summary>
    /// Invoke delegate
    /// </summary>
    /// <param name="context">The HttpContext of the request</param>
    public async Task Invoke(HttpContext context)
    {
        try
        {
            await _next(context);
        }
        catch (Exception error)
        {
            var response = context.Response;
            response.ContentType = "application/json";

            //TODO handler to check if has custom handler or to handle with base one
            switch (error)
            {
                case ValidationCwbException:
                    response.StatusCode = (int)HttpStatusCode.BadRequest;
                    break;
                case NotFoundCwbException:
                    response.StatusCode = (int)HttpStatusCode.NotFound;
                    break;
                default:
                    response.StatusCode = (int)HttpStatusCode.InternalServerError;
                    break;
            }

            var result = JsonSerializer.Serialize(new ExceptionModel
                { Message = error.Message, StatusCode = response.StatusCode });
            await response.WriteAsync(result);
        }
    }
}