﻿using CookingWebApi.BLL.Contracts.Products;
using CookingWebApi.BLL.Facades;
using CookingWebApi.BLL.Models;
using CookingWebApi.DAL.Pagination;
using Microsoft.AspNetCore.Mvc;

namespace CookingWebApi.PL.Controllers;

/// <summary>
/// The controller for products
/// </summary>
[ApiController]
[Route("[controller]")]
public class ProductsController : Controller
{
    private readonly ILogger<ProductsController> _logger;
    private readonly ProductsFacade _productsFacade;

    /// <summary>
    /// The ctor for <see cref="ProductsController"/> class
    /// </summary>
    /// <param name="logger"></param>
    /// <param name="productsFacade"></param>
    public ProductsController(ILogger<ProductsController> logger, ProductsFacade productsFacade)
    {
        _productsFacade = productsFacade;
        _logger = logger;
    }

    /// <summary>
    /// Get list of products.
    /// </summary>
    [HttpGet]
    public PagedResponse<ProductModel> GetList(string? filterBy, string? filterValue, int offset=0, int limit=50,
        string sortBy = "Id")
    {
        List<ProductModel> list;
        if (filterBy == null || filterValue == null)
            list = _productsFacade.GetBulkPaged(limit, offset, sortBy);
        else
            list = _productsFacade.GetBulkPagedWithFilter(limit, offset, filterBy, filterValue, sortBy);

        return new PagedResponse<ProductModel>(list,20,50,1);
    }

    /// <summary>
    /// Get product by id.
    /// </summary>
    /// <param name="id">The id.</param>
    [HttpGet("{id}")]
    public async Task<ProductModel?> GetById(Guid id)
    {
        var model = await _productsFacade.GetByIdAsync(id);

        return model;
    }

    /// <summary>
    /// Add new product.
    /// </summary>
    [HttpPost]
    public async Task<ProductModel> Add(ProductBody body)
    {
        var model = await _productsFacade.Add(body);

        return model;
    }
    
    /// <summary>
    /// Add products bulk.
    /// </summary>
    [HttpPost("bulk")]
    public async Task<List<Guid>> AddBulk(List<ProductBody> content)
    {
        var guids = await _productsFacade.AddBulk(content);

        return guids;
    }

    /// <summary>
    /// Update product by id.
    /// </summary>
    [HttpPut("{id}")]
    public async Task<ActionResult> Update(Guid id, ProductBody body)
    {
        await _productsFacade.Update(id, body);

        return Ok();
    }

    /// <summary>
    /// Delete product by id.
    /// </summary>
    [HttpDelete("{id}")]
    public async Task<ActionResult> Delete(Guid id)
    {
        await _productsFacade.DeleteByIdAsync(id);

        return Ok();
    }
}