﻿using CookingWebApi.BLL.Contracts.Recipe;
using CookingWebApi.BLL.Facades;
using CookingWebApi.BLL.Models;
using CookingWebApi.DAL.Pagination;
using Microsoft.AspNetCore.Mvc;

namespace CookingWebApi.PL.Controllers;

/// <summary>
/// The controller for recipes
/// </summary>
[ApiController]
[Route("[controller]")]
public class RecipeController : Controller
{
    private readonly ILogger<RecipeController> _logger;
    private readonly RecipeFacade _recipeFacade;

    /// <summary>
    /// The ctor for <see cref="RecipeController"/> class
    /// </summary>
    /// <param name="logger"></param>
    /// <param name="recipeFacade"></param>
    public RecipeController(ILogger<RecipeController> logger, RecipeFacade recipeFacade)
    {
        _recipeFacade = recipeFacade;
        _logger = logger;
    }

    /// <summary>
    /// Get list of recipes.
    /// </summary>
    [HttpGet]
    public List<RecipeModel> GetBulk(string? filterBy, string? filterValue)
    {
        List<RecipeModel> list = _recipeFacade.GetBulk(filterBy, filterValue);

        return list;
    }

    /// <summary>   
    /// Get list of recipes paged.
    /// </summary>
    [HttpGet("paged")]
    public PagedResponse<GetRecipePagedResponse> GetBulkPaged(int pageNumber, int pageSize, string? filterBy, string? filterValue,
        string sortBy = "Title")
    {
        var offset = pageSize * (pageNumber - 1);

        List<GetRecipePagedResponse> list;
        if (filterBy == null || filterValue == null)
            list = _recipeFacade.GetBulkPagedWithDetails(pageSize, offset, sortBy);
        else
            list = _recipeFacade.GetBulkPagedWithFilterAndDetails(pageSize, offset, filterBy, filterValue, sortBy);

        var totalCount = _recipeFacade.GetCount();


        return new PagedResponse<GetRecipePagedResponse>(list, totalCount, pageSize, pageNumber);
    }

    /// <summary>
    /// Get recipe by id.
    /// </summary>
    /// <param name="id">The id.</param>
    [HttpGet("{id}")]
    public async Task<RecipeModel?> GetById(Guid id)
    {
        var model = await _recipeFacade.GetByIdAsync(id);

        return model;
    }

    /// <summary>
    /// Get recipe by title.
    /// </summary>
    [HttpGet("title/{title}")]
    public async Task<GetRecipeFullDetailsResponse?> GetByTitle(string title)
    {
        var model = await _recipeFacade.GetByTitleAsync(title);

        return model;
    }
    
    /// <summary>
    /// Get multiple recipes by title match.
    /// </summary>
    [HttpGet("search/{title}")]
    public PagedResponse<GetRecipePagedResponse> GetMultipleByTitleMatch(string title, int pageNumber, int pageSize)
    {
        var offset = pageSize * (pageNumber - 1);

        var list = _recipeFacade.GetMultipleByTitleMatch(title, pageSize, offset);

        return new PagedResponse<GetRecipePagedResponse>(list.List, list.Count, pageSize, pageNumber);
    }

    /// <summary>
    /// Add new recipe.
    /// </summary>
    [HttpPost]
    public async Task<GetRecipeFullDetailsResponse> Add(AddRecipeRequest body)
    {
        var model = await _recipeFacade.AddRecipe(body);

        return model;
    }

    /// <summary>
    /// Include recipe to provided categories.
    /// </summary>
    [HttpPost("categories/id")]
    public async Task<GetRecipeFullDetailsResponse> Add(Guid id, List<string> categoriesNames)
    {
        var model = await _recipeFacade.AddRecipeToCategories(id, categoriesNames);

        return model;
    }

    /// <summary>
    /// Update recipe by id.
    /// </summary>
    [HttpPut("{id}")]
    public async Task<ActionResult> Update(Guid id, AddRecipeRequest body)
    {
        await _recipeFacade.UpdateRecipe(id, body);

        return Ok();
    }

    /// <summary>
    /// Delete recipe by id.
    /// </summary>
    [HttpDelete("{id}")]
    public async Task<ActionResult> Delete(Guid id)
    {
        await _recipeFacade.DeleteByIdAsync(id);

        return Ok();
    }
}