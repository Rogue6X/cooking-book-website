﻿using CookingWebApi.BLL.Contracts.Ingredients;
using CookingWebApi.BLL.Facades;
using CookingWebApi.BLL.Models;
using CookingWebApi.DAL.Pagination;
using Microsoft.AspNetCore.Mvc;

namespace CookingWebApi.PL.Controllers;

/// <summary>
/// The controller for ingredients
/// </summary>
[ApiController]
[Route("[controller]")]
public class IngredientsController : Controller
{
    private readonly ILogger<IngredientsController> _logger;
    private readonly IngredientsFacade _ingredientsFacade;

    /// <summary>
    /// The ctor for <see cref="IngredientsController"/> class
    /// </summary>
    /// <param name="logger"></param>
    /// <param name="ingredientsFacade"></param>
    public IngredientsController(ILogger<IngredientsController> logger, IngredientsFacade ingredientsFacade)
    {
        _ingredientsFacade = ingredientsFacade;
        _logger = logger;
    }

    /// <summary>
    /// Get list of ingredients.
    /// </summary>
    [HttpGet]
    public PagedResponse<IngredientModel> GetList(string? filterBy, string? filterValue, int offset = 0, int limit = 50,
        string sortBy = "Id")
    {
        List<IngredientModel> list;
        if (filterBy == null || filterValue == null)
            list = _ingredientsFacade.GetBulkPaged(limit, offset, sortBy);
        else
            list = _ingredientsFacade.GetBulkPagedWithFilter(limit, offset, filterBy, filterValue, sortBy);

        return new PagedResponse<IngredientModel>(list, 20, 50, 1);
    }

    /// <summary>
    /// Get ingredient by id.
    /// </summary>
    /// <param name="id">The id.</param>
    [HttpGet("{id}")]
    public async Task<IngredientModel?> GetById(Guid id)
    {
        var model = await _ingredientsFacade.GetByIdAsync(id);

        return model;
    }

    /// <summary>
    /// Get ingredients by recipe id.
    /// </summary>
    /// <param name="id">The id.</param>
    [HttpGet("recipe/{id}")]
    public IEnumerable<IngredientModel>? GetByRecipeId(Guid id)
    {
        var model = _ingredientsFacade.GetByRecipeId(id);

        return model;
    }

    /// <summary>
    /// Add new ingredient.
    /// </summary>
    [HttpPost]
    public async Task<IngredientModel> Add(IngredientBody body)
    {
        var model = await _ingredientsFacade.Add(body);

        return model;
    }

    /// <summary>
    /// Add multiple ingredients at once.
    /// </summary>
    [HttpPost("bulk/{recipeId}")]
    public async Task<List<Guid>> AddBulk(Guid recipeId, [FromBody] List<IngredientUpdateBody> content)
    {
        var guids = await _ingredientsFacade.AddBulk(recipeId, content);

        return guids;
    }

    /// <summary>
    /// Update ingredient by id.
    /// </summary>
    [HttpPut("{id}")]
    public async Task<ActionResult> Update(Guid id, IngredientUpdateBody body)
    {
        await _ingredientsFacade.Update(id, body);

        return Ok();
    }

    /// <summary>
    /// Delete ingredient by id.
    /// </summary>
    [HttpDelete("{id}")]
    public async Task<ActionResult> Delete(Guid id)
    {
        await _ingredientsFacade.DeleteByIdAsync(id);

        return Ok();
    }
}