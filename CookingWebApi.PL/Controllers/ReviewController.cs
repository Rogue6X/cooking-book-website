﻿using CookingWebApi.BLL.Contracts.Reviews;
using CookingWebApi.BLL.Facades;
using CookingWebApi.BLL.Models;
using CookingWebApi.DAL.Pagination;
using Microsoft.AspNetCore.Mvc;

namespace CookingWebApi.PL.Controllers;

/// <summary>
/// The controller for reviews
/// </summary>
[ApiController]
[Route("[controller]")]
public class ReviewController : Controller
{
    private readonly ILogger<ReviewController> _logger;
    private readonly ReviewFacade _reviewFacade;

    /// <summary>
    /// The ctor for <see cref="ReviewController"/> class
    /// </summary>
    /// <param name="logger"></param>
    /// <param name="reviewFacade"></param>
    public ReviewController(ILogger<ReviewController> logger, ReviewFacade reviewFacade)
    {
        _reviewFacade = reviewFacade;
        _logger = logger;
    }

    /// <summary>
    /// Get reviews for the recipe paged.
    /// </summary>
    [HttpGet("paged")]
    public async Task<PagedResponse<ReviewModel>> GetPaged(Guid recipeId, int pageNumber, int pageSize,
        string sortBy = "Created")
    {
        var offset = pageSize * (pageNumber - 1);

        var list = _reviewFacade.GetBulkPaged(pageSize, offset, sortBy);
        var totalCount = await _reviewFacade.GetRecipesReviewCount(recipeId);

        return new PagedResponse<ReviewModel>(list, totalCount, pageSize, pageNumber);
    }

    /// <summary>
    /// Get review by id.
    /// </summary>
    /// <param name="id">The id.</param>
    [HttpGet("{id}")]
    public async Task<ReviewModel?> GetById(Guid id)
    {
        var model = await _reviewFacade.GetByIdAsync(id);

        return model;
    }

    /// <summary>
    /// Get reviews by recipe id.
    /// </summary>
    [HttpGet("recipe/{recipeId}")]
    public async Task<IEnumerable<ReviewModel>?> GetReviewsByRecipe(Guid recipeId)
    {
        var model = await _reviewFacade.GetReviewsForRecipe(recipeId);

        return model;
    }

    /// <summary>
    /// Add new review.
    /// </summary>
    [HttpPost]
    public async Task<ReviewModel> Add(ReviewBody body)
    {
        var model = await _reviewFacade.Add(body);

        return model;
    }

    /// <summary>
    /// Update review by id.
    /// </summary>
    [HttpPut("{id}")]
    public async Task<ActionResult> Update(Guid id, ReviewBody body)
    {
        await _reviewFacade.Update(id, body);

        return Ok();
    }

    /// <summary>
    /// Delete review by id.
    /// </summary>
    [HttpDelete("{id}")]
    public async Task<ActionResult> Delete(Guid id)
    {
        await _reviewFacade.DeleteByIdAsync(id);

        return Ok();
    }

    /// <summary>
    /// Get count of reviews in recipe.
    /// </summary>
    [HttpGet("recipe/{id}/count")]
    public async Task<int> GetRecipesReviewCount(Guid id)
    {
        var count = await _reviewFacade.GetRecipesReviewCount(id);

        return count;
    }
}