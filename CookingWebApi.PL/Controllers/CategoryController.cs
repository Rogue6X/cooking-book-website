﻿using CookingWebApi.BLL.Contracts.Category;
using CookingWebApi.BLL.Facades;
using CookingWebApi.BLL.Models;
using CookingWebApi.DAL.Entities;
using CookingWebApi.DAL.Pagination;
using Microsoft.AspNetCore.Mvc;

namespace CookingWebApi.PL.Controllers;

/// <summary>
/// The controller for categories
/// </summary>
[ApiController]
[Route("[controller]")]
public class CategoryController : Controller
{
    private readonly ILogger<CategoryController> _logger;
    private readonly CategoryFacade _categoryFacade;

    /// <summary>
    /// The ctor for <see cref="CategoryController"/> class
    /// </summary>
    /// <param name="logger"></param>
    /// <param name="categoryFacade"></param>
    public CategoryController(ILogger<CategoryController> logger, CategoryFacade categoryFacade)
    {
        _categoryFacade = categoryFacade;
        _logger = logger;
    }

    /// <summary>
    /// Get categories paged.
    /// </summary>
    [HttpGet("paged")]
    public PagedResponse<CategoryListModel> GetPaged(int pageNumber, int pageSize)
    {
        var offset = pageSize * (pageNumber - 1);

        var list = _categoryFacade.GetBulk(pageSize, offset);
        var totalCount = _categoryFacade.GetCount();


        return new PagedResponse<CategoryListModel>(list, totalCount, pageSize, pageNumber);
    }

    /// <summary>
    /// Get category by id.
    /// </summary>
    /// <param name="id">The id.</param>
    [HttpGet("{id}")]
    public async Task<CategoryModel?> GetById(Guid id)
    {
        var model = await _categoryFacade.GetByIdAsync(id);

        return model;
    }

    /// <summary>
    /// Get paged recipes by category.
    /// </summary>
    /// <param name="name"></param>
    /// <param name="pageNumber"></param>
    /// <param name="pageSize"></param>
    /// <param name="sortBy"></param>
    /// <returns></returns>
    [HttpGet("recipes/{name}/paged")]
    public async Task<GetCategoryRecipesPaged> GetRecipesByCategoryPaged(string name, int pageNumber, int pageSize)
    {
        var offset = pageSize * (pageNumber - 1);
        var (model, count) = await _categoryFacade.GetRecipesFromCategoryPaged(name, pageSize, offset);

        return new GetCategoryRecipesPaged
            { Category = model, Page = new PaginationHeader(pageNumber, pageSize, count) };
    }

    /// <summary>
    /// Get categories with one recipe for each.
    /// </summary>
    /// <param name="pageSize"></param>
    [HttpGet("{pageSize}/thumbnail")]
    public List<CategoryListModel> GetCategoriesWithThumbnail(string pageSize)
    {
        var models = _categoryFacade.GetCategoriesWithThumbnail(int.Parse(pageSize));

        return models;
    }

    /// <summary>
    /// Get recipes by category.
    /// </summary>
    /// <param name="name"></param>
    /// <returns></returns>
    [HttpGet("recipes/{id}")]
    public async Task<IEnumerable<RecipeModel>?> GetRecipesByCategory(string name)
    {
        var model = await _categoryFacade.GetRecipesFromCategory(name);

        return model;
    }

    /// <summary>
    /// Get categories by recipe id.
    /// </summary>
    /// <param name="id">The id.</param>
    // [HttpGet("recipe/{id}")]
    // public IEnumerable<CategoryModel>? GetByRecipeId(Guid id)
    // {
    //     var model = _categoryFacade.GetByRecipeId(id);
    //
    //     return model;
    // }

    /// <summary>
    /// Add new category.
    /// </summary>
    [HttpPost]
    public async Task<CategoryModel> Add(CategoryBody body)
    {
        var model = await _categoryFacade.Add(body);

        return model;
    }

    /// <summary>
    /// Update category by id.
    /// </summary>
    [HttpPut("{id}")]
    public async Task<ActionResult> Update(Guid id, CategoryBody body)
    {
        await _categoryFacade.Update(id, body);

        return Ok();
    }

    /// <summary>
    /// Delete category by id.
    /// </summary>
    [HttpDelete("{id}")]
    public async Task<ActionResult> Delete(Guid id)
    {
        await _categoryFacade.DeleteByIdAsync(id);

        return Ok();
    }

    /// <summary>
    /// Get count of recipes in category.
    /// </summary>
    [HttpGet("{id}/recipes/count")]
    public async Task<int> GetCategoriesRecipesCount(Guid id)
    {
        var count = await _categoryFacade.GetCategoriesRecipesCount(id);

        return count;
    }
}