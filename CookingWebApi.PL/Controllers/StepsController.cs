﻿using CookingWebApi.BLL.Contracts.Steps;
using CookingWebApi.BLL.Facades;
using CookingWebApi.BLL.Models;
using CookingWebApi.DAL.Pagination;
using Microsoft.AspNetCore.Mvc;

namespace CookingWebApi.PL.Controllers;

/// <summary>
/// The controller for steps
/// </summary>
[ApiController]
[Route("[controller]")]
public class StepsController : Controller
{
    private readonly ILogger<StepsController> _logger;
    private readonly StepsFacade _stepsFacade;

    /// <summary>
    /// The ctor for <see cref="StepsController"/> class
    /// </summary>
    /// <param name="logger"></param>
    /// <param name="stepsFacade"></param>
    public StepsController(ILogger<StepsController> logger, StepsFacade stepsFacade)
    {
        _stepsFacade = stepsFacade;
        _logger = logger;
    }

    /// <summary>
    /// Get list of steps.
    /// </summary>
    [HttpGet]
    public PagedResponse<StepModel> GetList(string? filterBy, string? filterValue, int offset = 0, int limit = 50,
        string sortBy = "Id")
    {
        List<StepModel> list;
        if (filterBy == null || filterValue == null)
            list = _stepsFacade.GetBulkPaged(limit, offset, sortBy);
        else
            list = _stepsFacade.GetBulkPagedWithFilter(limit, offset, filterBy, filterValue, sortBy);

        return new PagedResponse<StepModel>(list, 20, 50, 1);
    }

    /// <summary>
    /// Get step by id.
    /// </summary>
    /// <param name="id">The id.</param>
    [HttpGet("{id}")]
    public async Task<StepModel?> GetById(Guid id)
    {
        var model = await _stepsFacade.GetByIdAsync(id);

        return model;
    }

    /// <summary>
    /// Get steps by recipe id.
    /// </summary>
    /// <param name="id">The id.</param>
    [HttpGet("recipe/{id}")]
    public IEnumerable<StepModel>? GetByRecipeId(Guid id)
    {
        var model = _stepsFacade.GetByRecipeIdAsync(id);

        return model;
    }

    /// <summary>
    /// Add new steps to the recipe.
    /// </summary>
    [HttpPost("{recipeId}")]
    public async Task<List<StepModel>> Add(Guid recipeId, List<StepBody> steps)
    {
        var model = await _stepsFacade.Add(recipeId, steps);

        return model;
    }

    /// <summary>
    /// Update step by id.
    /// </summary>
    [HttpPut("{id}")]
    public async Task<ActionResult> Update(Guid id, StepUpdateBody body)
    {
        await _stepsFacade.Update(id, body);

        return Ok();
    }

    /// <summary>
    /// Delete step by id.
    /// </summary>
    [HttpDelete("{id}")]
    public async Task<ActionResult> Delete(Guid id)
    {
        await _stepsFacade.DeleteByIdAsync(id);

        return Ok();
    }
}