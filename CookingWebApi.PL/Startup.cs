﻿using CookingWebApi.BLL;

namespace CookingWebApi.PL;

public static class Startup
{
    /// <summary>
    /// Configure PL services
    /// </summary>
    /// <param name="services"></param>
    /// <param name="configuration"></param>
    public static void ConfigurePresentationLayerServices(this IServiceCollection services, IConfiguration configuration)
    {
        services.ConfigureBusinessLayerServices(configuration);
    }
}