﻿using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace CookingWebApi.DAL.Entities;

/// <summary>
/// The ingredient for ingredient list
/// </summary>
public class Ingredient
{
    /// <summary>
    /// The Id
    /// </summary>
    public Guid Id { get; set; }

    /// <summary>
    /// The recipe id
    /// </summary>
    [ForeignKey("Id")]
    public Guid RecipeId { get; set; }
    
    /// <summary>
    /// Amount of product
    /// </summary>
    public float Amount { get; set; }

    /// <summary>
    /// Unit of product
    /// </summary>
    public string Unit { get; set; }

    /// <summary>
    /// The notes about specifics of the ingredient.
    /// </summary>
    public string? Notes { get; set; }

    /// <summary>
    /// The product id
    /// </summary>
    public Guid ProductId { get; set; }
    
    /// <summary>
    /// The product
    /// </summary>
    public Product Product { get; set; }
}