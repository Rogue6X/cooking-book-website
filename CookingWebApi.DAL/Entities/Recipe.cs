﻿using System.ComponentModel.DataAnnotations.Schema;

namespace CookingWebApi.DAL.Entities;

/// <summary>
/// The recipe entity
/// </summary>
public class Recipe
{
    public Guid Id { get; set; }

    public string Title { get; set; }

    /// <summary>
    /// The title used for routing on the client
    /// </summary>
    [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
    public string UrlTitle { get; set; }

    public Uri? OriginalLink { get; set; }

    public int? PrepareTime { get; set; }

    public int? CookTime { get; set; }

    [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
    public int? TotalTime { get; set; }
    
    public float? Rating { get; set; }

    public string? Description { get; set; }

    public float? Yield { get; set; }

    public string? PhotoUrl { get; set; }

    public DateTime Created { get; set; } = DateTime.UtcNow;

    public DateTime? Updated { get; set; } = DateTime.UtcNow;

    public ICollection<Ingredient> Ingredients { get; set; }

    public ICollection<Step> Steps { get; set; }

    public ICollection<Category> Categories { get; set; }

    public ICollection<Review> Reviews { get; set; }
}