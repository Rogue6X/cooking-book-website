﻿using System.ComponentModel.DataAnnotations.Schema;

namespace CookingWebApi.DAL.Entities;

public class Step
{
    public Guid Id { get; set; }

    [ForeignKey("Id")]
    public Guid RecipeId { get; set; }

    public int StepNumber { get; set; }

    public string Description { get; set; }

    public List<Uri>?PhotoUrls { get; set; }
    public Recipe Recipe { get; set; }
}