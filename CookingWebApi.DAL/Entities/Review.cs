﻿namespace CookingWebApi.DAL.Entities;

/// <summary>
/// The review of the recipe
/// </summary>
public class Review
{
    public Guid Id { get; set; }

    public Guid RecipeId { get; set; }

    public string Author { get; set; }

    public float Rating { get; set; }

    public string Text { get; set; }

    public DateTime Created { get; set; } = DateTime.UtcNow;

    public Recipe Recipe { get; set; }
}