﻿using System.ComponentModel.DataAnnotations.Schema;

namespace CookingWebApi.DAL.Entities;

/// <summary>
/// The category entity
/// </summary>
public class Category
{
    public Guid Id { get; set; }

    public string Name { get; set; }
    
    [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
    public string UrlName { get; private set; }

    public ICollection<Recipe>? Recipes { get; set; }
}