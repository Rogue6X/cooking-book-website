﻿namespace CookingWebApi.DAL.Entities;

public class Product
{
    public Guid Id { get; set; }

    public string Name { get; set; }

    public List<string>? Substitutes { get; set; }
}