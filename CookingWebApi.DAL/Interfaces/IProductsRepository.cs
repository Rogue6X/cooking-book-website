﻿using CookingWebApi.DAL.Entities;

namespace CookingWebApi.DAL.Interfaces;

/// <summary>
/// The repository for products
/// </summary>
public interface IProductsRepository : IBaseRepository<Product>
{
    Task<Product?> GetByNameAsync(string name);
}