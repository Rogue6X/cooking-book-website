﻿using CookingWebApi.DAL.Entities;

namespace CookingWebApi.DAL.Interfaces;

public interface IIngredientsRepository : IBaseRepository<Ingredient>
{
    public IQueryable<Ingredient> GetByRecipeId(Guid id);
}