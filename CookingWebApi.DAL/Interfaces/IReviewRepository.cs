﻿using System.Linq.Expressions;
using CookingWebApi.DAL.Entities;

namespace CookingWebApi.DAL.Interfaces;

public interface IReviewRepository : IBaseRepository<Review>
{
    /// <summary>
    /// Get bulk entities paged
    /// </summary>
    /// <param name="id">Id of the recipe</param>
    /// <param name="limit">Amount to take</param>
    /// <param name="offset">Amount to skip</param>
    /// <param name="order">Order by condition</param>
    IEnumerable<Review?> GetBulkPaged(Guid id, int limit, int offset, string order);
}