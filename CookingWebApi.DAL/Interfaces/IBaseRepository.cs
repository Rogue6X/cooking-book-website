﻿using System.Linq.Expressions;

namespace CookingWebApi.DAL.Interfaces;

/// <summary>
/// The base class for repositories
/// </summary>
public interface IBaseRepository<T>
{
    /// <summary>
    /// Get bulk entities paged
    /// </summary>
    /// <param name="limit">Amount to take</param>
    /// <param name="offset">Amount to skip</param>
    /// <param name="order">Order by condition</param>
    /// <param name="filter">Filter condition</param>
    IEnumerable<T?> GetBulkPaged(int limit, int offset, string order,
        Expression<Func<T, bool>>? filter);
    
    /// <summary>
    /// Get bulk entities with filter
    /// </summary>
    /// <param name="filter"></param>
    /// <returns></returns>
    IEnumerable<T?> GetBulk(Expression<Func<T, bool>>? filter);

    /// <summary>
    /// Get entity by id
    /// </summary>
    /// <param name="id">The id of entity</param>
    Task<T?> GetByIdAsync(Guid id);

    /// <summary>
    /// Add entity async
    /// </summary>
    /// <param name="entity">The entity</param>
    Task<T> AddAsync(T entity);

    /// <summary>
    /// Delete provided entity
    /// </summary>
    /// <param name="entity">The entity</param>
    void Delete(T entity);

    /// <summary>
    /// Gets the count of all entities in table
    /// </summary>
    /// <returns></returns>
    int Count();
}