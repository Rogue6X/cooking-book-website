﻿using CookingWebApi.DAL.Entities;
using CookingWebApi.DAL.Pagination;

namespace CookingWebApi.DAL.Interfaces;

/// <summary>
/// The repository for recipes
/// </summary>
public interface IRecipeRepository : IBaseRepository<Recipe>
{
    Task<Recipe?> GetByNameAsync(string title);
    
    CountedList<Recipe>? GetMultipleByName(string title, int limit, int offset);
}