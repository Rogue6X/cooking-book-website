﻿using System.Linq.Expressions;
using CookingWebApi.DAL.Entities;

namespace CookingWebApi.DAL.Interfaces;

/// <summary>
/// The category repository
/// </summary>
public interface ICategoryRepository : IBaseRepository<Category>
{
    /// <summary>
    /// Get category with recipes
    /// </summary>
    /// <param name="name">The category name</param>
    Task<Category?> GetCategoryWithRecipes(string name);

    /// <summary>
    /// Get categories with recipes
    /// </summary>
    /// <param name="filter">The filter</param>
    IQueryable<Category> GetCategoriesWithRecipes(Expression<Func<Category, bool>> filter);

    /// <summary>
    /// Get categories with one recipe for each category
    /// </summary>
    /// <param name="limit">The limit</param>
    public IQueryable<Category> GetCategoriesWithThumbnailRecipe(int limit);

    public IQueryable<Category> GetBulkPaged(int limit, int offset);
}