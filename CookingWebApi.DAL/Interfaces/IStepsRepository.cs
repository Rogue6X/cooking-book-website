﻿using CookingWebApi.DAL.Entities;

namespace CookingWebApi.DAL.Interfaces;

/// <summary>
/// The repository for cooking steps
/// </summary>
public interface IStepsRepository : IBaseRepository<Step>
{
    public IQueryable<Step> GetByRecipeId(Guid id);
}