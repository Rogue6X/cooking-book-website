﻿using System.Linq.Expressions;
using System.Reflection;

namespace CookingWebApi.DAL.Utils;

/// <summary>
/// Extensions for ordering by provided string
/// </summary>
public static class OrderByExtensions
{
    public static IOrderedQueryable<T>? CustomOrderBy<T>(this IQueryable<T> source, string orderBy)
    {
        var orders = orderBy.Split(";").ToList();
        var pairs = orders.Select(x =>
        {
            var split = x.Split(".");
            return new { Property = split[0], Direction = split.Length != 1 ? split[1] : "asc" };
        }).ToList();
        var result = source.CustomOrderBy(pairs[0].Property, pairs[0].Direction);

        if (pairs.Count > 1)
        {
            for (int i = 1; i < pairs.Count; i++)
            {
                result = result.CustomThenOrderBy(pairs[i].Property, pairs[i].Direction);
            }
        }

        return result;
    }

    private static IOrderedQueryable<T>? CustomOrderBy<T>(
        this IQueryable<T> source,
        string property, string direction)
    {
        var methodName = "OrderBy";
        if (direction == "desc")
        {
            methodName += "Descending";
        }

        return ApplyOrder(source, property, methodName);
    }

    private static IOrderedQueryable<T>? CustomThenOrderBy<T>(
        this IQueryable<T> source,
        string property, string direction)
    {
        var methodName = "ThenBy";
        if (direction == "desc")
        {
            methodName += "Descending";
        }

        return ApplyOrder(source, property, methodName);
    }

    private static IOrderedQueryable<T>? OrderBy<T>(
        this IQueryable<T> source,
        string property)
    {
        return ApplyOrder(source, property, "OrderBy");
    }

    private static IOrderedQueryable<T>? OrderByDescending<T>(
        this IQueryable<T> source,
        string property)
    {
        return ApplyOrder(source, property, "OrderByDescending");
    }

    private static IOrderedQueryable<T>? ThenBy<T>(
        this IOrderedQueryable<T> source,
        string property)
    {
        return ApplyOrder(source, property, "ThenBy");
    }

    private static IOrderedQueryable<T>? ThenByDescending<T>(
        this IOrderedQueryable<T> source,
        string property)
    {
        return ApplyOrder(source, property, "ThenByDescending");
    }

    private static IOrderedQueryable<T>? ApplyOrder<T>(
        IQueryable<T> source,
        string property,
        string methodName)
    {
        var props = property.Split('.');
        var type = typeof(T);
        var arg = Expression.Parameter(type, "x");
        Expression expr = arg;
        foreach (string prop in props)
        {
            // use reflection (not ComponentModel) to mirror LINQ
            PropertyInfo? pi = type.GetProperty(prop);
            expr = Expression.Property(expr, pi);
            type = pi.PropertyType;
        }

        var delegateType = typeof(Func<,>).MakeGenericType(typeof(T), type);
        var lambda = Expression.Lambda(delegateType, expr, arg);

        var result = typeof(Queryable).GetMethods().Single(
                method => method.Name == methodName
                          && method.IsGenericMethodDefinition
                          && method.GetGenericArguments().Length == 2
                          && method.GetParameters().Length == 2)
            .MakeGenericMethod(typeof(T), type)
            .Invoke(null, new object[] { source, lambda });
        return (IOrderedQueryable<T>)result;
    }
}