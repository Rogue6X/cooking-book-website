﻿namespace CookingWebApi.DAL.Pagination;

public class CountedList<T>
{
    public List<T> List { get; set; }

    public int Count { get; set; }

    public CountedList(List<T> elements, int count)
    {
        List = elements;
        Count = count;
    }
}