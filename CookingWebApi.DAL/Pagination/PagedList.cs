﻿namespace CookingWebApi.DAL.Pagination
{
    /// <summary>
    /// Paged list for pagination
    /// </summary>
    /// <typeparam name="T">Returned entities type</typeparam>
    public class PagedResponse<T>
    {
        public List<T> List { get; set; }

        public PagedResponse(IEnumerable<T> items, int count, int pageSize, int pageNumber)
        {
            Page = new PaginationHeader(pageNumber, pageSize, count);
            List = new List<T>(items);
        }

        public PaginationHeader Page { get; set; }

        public static PagedResponse<T> CreateAsync(T[] source, int pageNumber, int pageSize)
        {
            var count = source.Length;
            var items = source.Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();
            return new PagedResponse<T>(items, count, pageSize, pageNumber);
        }
    }
}