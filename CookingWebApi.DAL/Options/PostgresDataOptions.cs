﻿namespace CookingWebApi.DAL.Options;

public class PostgresDataOptions
{
    public const string Path = "Postgres";

    public string Host { get; set; }
    public string Database { get; set; }
    public string User { get; set; }
    public string Password { get; set; }
    public int Port { get; set; }
}