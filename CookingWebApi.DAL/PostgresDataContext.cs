﻿using CookingWebApi.DAL.Entities;
using Microsoft.EntityFrameworkCore;

namespace CookingWebApi.DAL;

/// <summary>
/// Db context for Postgres Db
/// </summary>
public class PostgresDataContext : DbContext
{
    public PostgresDataContext(DbContextOptions options) : base(options)
    {
    }

    public DbSet<Recipe> Recipes { get; set; }
    
    public DbSet<Review> Reviews { get; set; }
    
    public DbSet<Ingredient> Ingredients { get; set; }

    public DbSet<Product> Products { get; set; }
    
    public DbSet<Step> Steps { get; set; }

    public DbSet<Category> Categories { get; set; }
}