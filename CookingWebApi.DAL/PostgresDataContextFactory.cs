﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace CookingWebApi.DAL;

public class PostgresDataContextFactory : IDesignTimeDbContextFactory<PostgresDataContext>
{
    public PostgresDataContext CreateDbContext(string[] args)
    {
        var optionsBuilder = new DbContextOptionsBuilder<PostgresDataContext>();
        optionsBuilder.UseNpgsql("Server=localhost;Port=5432;Database=CookingDb;User Id=develop;Password=password123;");

        return new PostgresDataContext(optionsBuilder.Options);
    }
}