﻿using CookingWebApi.DAL.Interfaces;
using CookingWebApi.DAL.Options;
using CookingWebApi.DAL.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Npgsql;

namespace CookingWebApi.DAL;

public static class Startup
{
    private const string Appsettings = "AppSettings";

    public static void ConfigureDataAccessLayerServices(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddDbContext<PostgresDataContext>(o =>
        {
            var stringBuilder = new NpgsqlConnectionStringBuilder();
            var appsettings = configuration.GetSection(Appsettings);
            var options = appsettings.GetSection(PostgresDataOptions.Path).Get<PostgresDataOptions>();
            stringBuilder.Database = options.Database;
            stringBuilder.Host = options.Host;
            stringBuilder.Password = options.Password;
            stringBuilder.Username = options.User;
            stringBuilder.Port = options.Port;
            o.UseNpgsql(stringBuilder.ConnectionString);
        });

        services.AddScoped<UoW>();
        services.AddScoped<IRecipeRepository, RecipeRepository>();
        services.AddScoped<IReviewRepository, ReviewRepository>();
        services.AddScoped<IIngredientsRepository, IngredientsRepository>();
        services.AddScoped<IStepsRepository, StepsRepository>();
        services.AddScoped<IProductsRepository, ProductsRepository>();
        services.AddScoped<ICategoryRepository, CategoryRepository>();
    }
}