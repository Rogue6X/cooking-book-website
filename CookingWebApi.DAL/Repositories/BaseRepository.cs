﻿using System.Linq.Expressions;
using CookingWebApi.DAL.Interfaces;
using CookingWebApi.DAL.Utils;
using Microsoft.EntityFrameworkCore;

namespace CookingWebApi.DAL.Repositories;

///<inheritdoc />
public class BaseRepository<T> : IBaseRepository<T> where T : class
{
    internal readonly DbSet<T> DbSet;

    public BaseRepository(PostgresDataContext context)
    {
        DbSet = context.Set<T>();
    }

    public virtual IEnumerable<T?> GetBulkPaged(int limit, int offset, string order, Expression<Func<T, bool>>? filter)
    {
        if (filter != null)
        {
            return DbSet.Where(filter).CustomOrderBy(order).Skip(offset).Take(limit);
        }

        return DbSet.CustomOrderBy(order).Skip(offset).Take(limit);
    }

    public IEnumerable<T?> GetBulk(Expression<Func<T, bool>>? filter)
    {
        return filter != null ? DbSet.Where(filter) : DbSet;
    }

    public virtual async Task<T?> GetByIdAsync(Guid id)
    {
        return await DbSet.FindAsync(id);
    }

    public async Task<T> AddAsync(T entity)
    {
        var result = await DbSet.AddAsync(entity);

        return result.Entity;
    }

    public void Delete(T entity)
    {
        DbSet.Remove(entity);
    }

    public int Count()
    {
        return DbSet.Count();
    }
}