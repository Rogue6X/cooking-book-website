﻿using CookingWebApi.DAL.Entities;
using CookingWebApi.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace CookingWebApi.DAL.Repositories;

///<inheritdoc cref="IRecipeRepository"/>
public class IngredientsRepository : BaseRepository<Ingredient>, IIngredientsRepository
{
    public IngredientsRepository(PostgresDataContext context) : base(context)
    {
    }

    public override async Task<Ingredient?> GetByIdAsync(Guid id)
    {
        return await DbSet
            .Include(x => x.Product)
            .FirstOrDefaultAsync(x => x.Id == id);
    }

    public IQueryable<Ingredient> GetByRecipeId(Guid id)
    {
        return DbSet.Where(i => i.RecipeId == id);
    }
}