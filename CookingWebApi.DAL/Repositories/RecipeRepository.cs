﻿using System.Linq.Expressions;
using CookingWebApi.DAL.Entities;
using CookingWebApi.DAL.Interfaces;
using CookingWebApi.DAL.Pagination;
using CookingWebApi.DAL.Utils;
using Microsoft.EntityFrameworkCore;

namespace CookingWebApi.DAL.Repositories;

///<inheritdoc cref="IRecipeRepository"/>
public class RecipeRepository : BaseRepository<Recipe>, IRecipeRepository
{
    public RecipeRepository(PostgresDataContext context) : base(context)
    {
    }

    public override IEnumerable<Recipe?> GetBulkPaged(int limit, int offset, string order,
        Expression<Func<Recipe, bool>>? filter)
    {
        if (filter != null)
        {
            return DbSet.Where(filter).CustomOrderBy(order).Skip(offset).Take(limit);
        }

        return DbSet.CustomOrderBy(order).Skip(offset).Take(limit);
    }

    public override async Task<Recipe?> GetByIdAsync(Guid id)
    {
        return await DbSet
            .Include(x => x.Ingredients)
            .Include(x => x.Reviews)
            .FirstOrDefaultAsync(x => x.Id == id);
    }

    public async Task<Recipe?> GetByNameAsync(string title)
    {
        return await DbSet
            .Include(x => x.Categories)
            .Include(x => x.Ingredients)
            .ThenInclude(i => i.Product)
            .Include(x => x.Steps.OrderBy(s => s.StepNumber))
            .Include(x=>x.Reviews.OrderBy(r=>r.Rating))
            .FirstOrDefaultAsync(x => x.UrlTitle == title);
    }

    public CountedList<Recipe> GetMultipleByName(string title, int limit, int offset)
    {
        var lowercaseTitle = title.ToLower();

        var byTitle = DbSet
            .Where(r => r.Title.ToLower().Contains(lowercaseTitle))
            .ToList();
        
        var byIngredients = DbSet
            .Include(r => r.Ingredients)
            .ThenInclude(i => i.Product)
            .Where(r => r.Ingredients.Any(i => 
                i.Product.Name.ToLower().Contains(lowercaseTitle)));
            

        byTitle.AddRange(byIngredients);
        var resultList = byTitle.Skip(offset).Take(limit);

        return new CountedList<Recipe>(resultList.ToList(), byTitle.Count);
    }
}