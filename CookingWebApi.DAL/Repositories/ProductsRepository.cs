﻿using CookingWebApi.DAL.Entities;
using CookingWebApi.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace CookingWebApi.DAL.Repositories;

///<inheritdoc cref="IRecipeRepository"/>
public class ProductsRepository : BaseRepository<Product>, IProductsRepository
{
    public ProductsRepository(PostgresDataContext context) : base(context)
    {
    }

    public async Task<Product?> GetByNameAsync(string name)
    {
        return await DbSet.FirstOrDefaultAsync(x => x.Name.ToLower() == name.ToLower());
    }
}