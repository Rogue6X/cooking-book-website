﻿using CookingWebApi.DAL.Entities;
using CookingWebApi.DAL.Interfaces;
using CookingWebApi.DAL.Utils;

namespace CookingWebApi.DAL.Repositories;

public class ReviewRepository : BaseRepository<Review>, IReviewRepository
{
    public ReviewRepository(PostgresDataContext context) : base(context)
    {
    }

    public IEnumerable<Review?> GetBulkPaged(Guid id, int limit, int offset, string order)
    {
        return DbSet.CustomOrderBy(order).Skip(offset).Take(limit);
    }
}