﻿using CookingWebApi.DAL.Entities;
using CookingWebApi.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace CookingWebApi.DAL.Repositories;

///<inheritdoc cref="IRecipeRepository"/>
public class StepsRepository : BaseRepository<Step>, IStepsRepository
{
    public StepsRepository(PostgresDataContext context) : base(context)
    {
    }

    public override async Task<Step?> GetByIdAsync(Guid id)
    {
        return await DbSet
            .Include(x => x.Recipe)
            .FirstOrDefaultAsync(x => x.Id == id);
    }

    public IQueryable<Step> GetByRecipeId(Guid id)
    {
        return DbSet.Where(s => s.RecipeId == id);
    }
}