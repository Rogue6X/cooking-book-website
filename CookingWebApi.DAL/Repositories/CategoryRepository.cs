﻿using System.Linq.Expressions;
using CookingWebApi.DAL.Entities;
using CookingWebApi.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace CookingWebApi.DAL.Repositories;

///<inheritdoc cref="ICategoryRepository"/>
public class CategoryRepository : BaseRepository<Category>, ICategoryRepository
{
    public CategoryRepository(PostgresDataContext context) : base(context)
    {
    }

    public override Task<Category?> GetByIdAsync(Guid id)
    {
        return DbSet.Include(x => x.Recipes).FirstOrDefaultAsync(x => x.Id == id);
    }

    public async Task<Category?> GetCategoryWithRecipes(string name)
    {
        var category = await DbSet
            .Include(c => c.Recipes)!
            .FirstOrDefaultAsync(c => c.UrlName == name);

        return category;
    }
    
    public IQueryable<Category> GetBulkPaged(int limit, int offset)
    {
        return DbSet.Include(x=>x.Recipes)
            .Skip(offset).Take(limit);
    }

    public IQueryable<Category> GetCategoriesWithRecipes(Expression<Func<Category, bool>> filter)
    {
        var collection = DbSet
            .Include(c => c.Recipes)
            .Where(filter);

        return collection;
    }
    
    public IQueryable<Category> GetCategoriesWithThumbnailRecipe(int limit)
    {
        var collection = DbSet
            .Include(c => c.Recipes.OrderByDescending(r=>r.Rating))
            .OrderBy(c=>c.UrlName)
            .Take(limit);

        return collection;
    }
}