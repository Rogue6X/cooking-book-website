﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CookingWebApi.DAL.Migrations
{
    /// <inheritdoc />
    public partial class MoveSummaryToRecipeEntity : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Summaries");

            migrationBuilder.AddColumn<int>(
                name: "CookTime",
                table: "Recipes",
                type: "integer",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "Recipes",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "PrepareTime",
                table: "Recipes",
                type: "integer",
                nullable: true);

            migrationBuilder.AddColumn<float>(
                name: "Rating",
                table: "Recipes",
                type: "real",
                nullable: true);

            migrationBuilder.AddColumn<float>(
                name: "Yield",
                table: "Recipes",
                type: "real",
                nullable: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CookTime",
                table: "Recipes");

            migrationBuilder.DropColumn(
                name: "Description",
                table: "Recipes");

            migrationBuilder.DropColumn(
                name: "PrepareTime",
                table: "Recipes");

            migrationBuilder.DropColumn(
                name: "Rating",
                table: "Recipes");

            migrationBuilder.DropColumn(
                name: "Yield",
                table: "Recipes");

            migrationBuilder.CreateTable(
                name: "Summaries",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    RecipeId = table.Column<Guid>(type: "uuid", nullable: false),
                    CookTime = table.Column<int>(type: "integer", nullable: true),
                    PrepareTime = table.Column<int>(type: "integer", nullable: true),
                    Rating = table.Column<float>(type: "real", nullable: true),
                    Review = table.Column<string>(type: "text", nullable: true),
                    Yield = table.Column<float>(type: "real", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Summaries", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Summaries_Recipes_RecipeId",
                        column: x => x.RecipeId,
                        principalTable: "Recipes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Summaries_RecipeId",
                table: "Summaries",
                column: "RecipeId",
                unique: true);
        }
    }
}
