﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CookingWebApi.DAL.Migrations
{
    /// <inheritdoc />
    public partial class RenameIngredientsNoteToNotes : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Note",
                table: "Ingredients",
                newName: "Notes");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Notes",
                table: "Ingredients",
                newName: "Note");
        }
    }
}
