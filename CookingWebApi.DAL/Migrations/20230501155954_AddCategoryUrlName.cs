﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CookingWebApi.DAL.Migrations
{
    /// <inheritdoc />
    public partial class AddCategoryUrlName : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "UrlName",
                table: "Categories",
                type: "text",
                nullable: false,
                computedColumnSql:"LOWER(REPLACE(\"Name\",' ','-'))",
                stored:true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "UrlName",
                table: "Categories");
        }
    }
}
