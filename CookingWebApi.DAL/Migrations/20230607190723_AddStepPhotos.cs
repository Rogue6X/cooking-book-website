﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CookingWebApi.DAL.Migrations
{
    /// <inheritdoc />
    public partial class AddStepPhotos : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string[]>(
                name: "PhotoUrls",
                table: "Steps",
                type: "text[]",
                nullable: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PhotoUrls",
                table: "Steps");
        }
    }
}
