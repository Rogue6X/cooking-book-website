﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CookingWebApi.DAL.Migrations
{
    /// <inheritdoc />
    public partial class AddNoteFieldToIngredients : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Note",
                table: "Ingredients",
                type: "text",
                nullable: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Note",
                table: "Ingredients");
        }
    }
}
