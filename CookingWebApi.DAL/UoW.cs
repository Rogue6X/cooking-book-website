﻿using CookingWebApi.DAL.Interfaces;

namespace CookingWebApi.DAL;

public class UoW
{
    private readonly PostgresDataContext _context;

    public UoW(PostgresDataContext context, IRecipeRepository recipeRepository, IReviewRepository reviewRepository,
        IIngredientsRepository ingredientsRepository, IStepsRepository stepsRepository,
        IProductsRepository productsRepository, ICategoryRepository categoryRepository)
    {
        RecipeRepository = recipeRepository;
        ReviewRepository = reviewRepository;
        IngredientsRepository = ingredientsRepository;
        StepsRepository = stepsRepository;
        ProductsRepository = productsRepository;
        CategoryRepository = categoryRepository;
        _context = context;
    }

    public IRecipeRepository RecipeRepository { get; }
    public IIngredientsRepository IngredientsRepository { get; }
    public IStepsRepository StepsRepository { get; }
    public IProductsRepository ProductsRepository { get; }
    public ICategoryRepository CategoryRepository { get; }
    public IReviewRepository ReviewRepository { get; }

    public async Task<bool> SaveAsync()
    {
        return await _context.SaveChangesAsync() > 0;
    }

    public void Dispose()
    {
        _context.Dispose();
    }
}