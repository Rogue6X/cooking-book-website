import { Component } from '@angular/core';
import { Category } from 'src/app/_models/category';
import { Recipe } from 'src/app/_models/recipe';
import { CategoryService } from 'src/app/_services/category.service';
import { RecipesService } from 'src/app/_services/recipes.service';

@Component({
  selector: 'app-recipes-main',
  templateUrl: './recipes-main.component.html',
  styleUrls: ['./recipes-main.component.css'],
})
export class RecipesMainComponent {
  hotRecipes: Recipe[];
  categories: Category[];

  constructor(
    private recipeService: RecipesService,
    private categoryService: CategoryService
  ) {}

  ngOnInit(): void {
    this.recipeService
      .getRecipesPaged(1, 9, 'Title')
      .subscribe((r) => (this.hotRecipes = r.list));
    this.categoryService
      .getCategoriesPagedWithThumbnail(9)
      .subscribe((c) => (this.categories = c));
  }
}
