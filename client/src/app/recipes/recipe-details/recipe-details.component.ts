import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Ingredient } from 'src/app/_models/ingredient';
import { Recipe } from 'src/app/_models/recipe';
import { RecipesService } from 'src/app/_services/recipes.service';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  ValidatorFn,
  Validators
} from "@angular/forms";
import { ReviewService } from 'src/app/_services/review.service';

@Component({
  selector: 'app-recipe-details',
  templateUrl: './recipe-details.component.html',
  styleUrls: ['./recipe-details.component.css'],
})
export class RecipeDetailsComponent implements OnInit {
  recipe: Recipe;
  ingredients: Ingredient[];
  commentForm: FormGroup;
  model: any = {rating:5}
  showSuccessText = false

  constructor(
    private recipeService: RecipesService,
    private route: ActivatedRoute,
    private reviewService: ReviewService
  ) { }

  ngOnInit(): void {
    this.loadRecipe();
  }

  loadRecipe() {
    const title = this.route.snapshot.paramMap.get('title');
    if (!title) {
      console.log("Can't retrive route parameter");
      return;
    }

    this.recipeService
      .getRecipeByName(title)
      .subscribe((r) => (this.recipe = r));
  }

  submitComment() {
    this.model.recipeId = this.recipe.id
    this.reviewService.addNewReview(this.model).subscribe(() =>
      this.showSuccessText = true)
  }
}
