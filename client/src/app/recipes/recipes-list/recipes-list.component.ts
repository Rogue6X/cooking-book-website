import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Category } from 'src/app/_models/category';
import { Pagination } from 'src/app/_models/pagination';
import { Recipe } from 'src/app/_models/recipe';
import { CategoryService } from 'src/app/_services/category.service';
import { RecipesService } from 'src/app/_services/recipes.service';

@Component({
  selector: 'app-recipes-list',
  templateUrl: './recipes-list.component.html',
  styleUrls: ['./recipes-list.component.css'],
})
export class RecipesListComponent implements OnInit {
  category: Category;
  categoryName: string = "Recipes"
  recipes: Recipe[];
  pageSize: number = 9;
  pageNumber: number = 1;
  sortBy: string = 'Updated.desc';
  filterBy: string;
  filterValue: string;
  pagination: Pagination;

  label: string;

  constructor(
    private recipeService: RecipesService,
    private categoryService: CategoryService,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.loadMembers();
  }

  loadMembers() {
    const categoryName = this.route.snapshot.paramMap.get('name');
    if (categoryName) {
      this.handleCategoryList(categoryName)
    } else {
      this.handleExploreList()
    }
  }

  handleCategoryList(categoryName: string) {
    this.categoryService.getCategoryWithPagedRecipes(categoryName, this.pageNumber, this.pageSize)
      .subscribe(r => {
        this.category = r.category
        this.categoryName = r.category.name;
        this.recipes = r.category.recipes;
        this.pagination = r.page
      })
  }

  handleExploreList() {
    this.recipeService
      .getRecipesPaged(
        this.pageNumber,
        this.pageSize,
        this.sortBy,
        this.filterBy,
        this.filterValue
      )
      .subscribe((r) => {
        this.recipes = r.list;
        this.pagination = r.page;
      });
  }

  pageChanged(event: any) {
    this.pageNumber = event.page;
    this.loadMembers();
  }

  sort(labelNumber: string) {
    switch (labelNumber) {
      case 'label1':
        this.sortBy = "Updated.desc";
        break;
      case 'label2':
        this.sortBy = "Rating.desc";
        break;
      case 'label3':
        this.sortBy = 'TotalTime';
        break;
    }
    this.loadMembers()
  }
}
