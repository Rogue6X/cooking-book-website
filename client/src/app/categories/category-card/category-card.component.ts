import { Component, Input, OnInit } from '@angular/core';
import { Category } from '../../_models/category';
import { Recipe } from '../../_models/recipe';
import { CategoryService } from '../../_services/category.service';

@Component({
  selector: 'app-category-card',
  templateUrl: './category-card.component.html',
  styleUrls: ['./category-card.component.css'],
})
export class CategoryCardComponent implements OnInit {
  @Input() category: Category;
  length: number;
  isActive = false;

  constructor(private categoryService: CategoryService) { }

  ngOnInit() {
    this.categoryService
      .getCategoriesRecipesCount(this.category.id)
      .subscribe((r) => {
        this.length = r;
      });
  }
}
