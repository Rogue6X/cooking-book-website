import { Component, OnInit } from '@angular/core';
import { Category } from 'src/app/_models/category';
import { Pagination } from 'src/app/_models/pagination';
import { CategoryService } from 'src/app/_services/category.service';

@Component({
  selector: 'app-category-list',
  templateUrl: './category-list.component.html',
  styleUrls: ['./category-list.component.css'],
})
export class CategoryListComponent implements OnInit {
  categories: Category[];
  pageSize: number = 9;
  pageNumber: number = 1;
  sortBy: string = 'Name';
  pagination: Pagination;

  constructor(private categoryService: CategoryService) {}

  ngOnInit(): void {
    this.loadMembers();
  }

  loadMembers() {
    this.categoryService
      .getCategoriesPaged(this.pageNumber, this.pageSize, this.sortBy)
      .subscribe((r) => {
        this.categories = r.list;
        this.pagination = r.page;
      });
  }

  pageChanged(event: any) {
    this.pageNumber = event.page;
    this.loadMembers();
  }
}
