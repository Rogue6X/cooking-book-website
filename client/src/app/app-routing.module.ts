import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutComponent } from './about/about.component';
import { RecipeDetailsComponent } from './recipes/recipe-details/recipe-details.component';
import { RecipesMainComponent } from './recipes/recipes-main/recipes-main.component';
import { HomeComponent } from './home/home.component';
import { RecipesListComponent } from './recipes/recipes-list/recipes-list.component';
import { CategoryListComponent } from './categories/category-list/category-list.component';
import { SearchListComponent } from './search-list/search-list.component';
import { ContactComponent } from './contact/contact.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'recipes', component: RecipesMainComponent },
  { path: "recipes/explore", component: RecipesListComponent },
  { path: "categories", component: CategoryListComponent },
  { path: 'about', component: AboutComponent },
  { path: 'recipes/:name', component: RecipesListComponent },
  { path: 'search/:query', component: SearchListComponent },
  { path: 'contact', component: ContactComponent },
  { path: ':title', component: RecipeDetailsComponent },
  // {path: "not-found", component: NotFoundComponent},
  // {path: "server-error", component: ServerErrorComponent},
  { path: '**', component: HomeComponent, pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
