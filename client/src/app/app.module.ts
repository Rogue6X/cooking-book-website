import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavbarComponent } from './navbar/navbar.component';
import { RecipesMainComponent } from './recipes/recipes-main/recipes-main.component';
import { RecipesListComponent } from './recipes/recipes-list/recipes-list.component';
import { RecipeCardComponent } from './recipes/recipe-card/recipe-card.component';
import { CategoryCardComponent } from './categories/category-card/category-card.component';
import { FooterComponent } from './footer/footer.component';
import { RecipeDetailsComponent } from './recipes/recipe-details/recipe-details.component';
import { AboutComponent } from './about/about.component';
import { HomeComponent } from './home/home.component';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { CategoryListComponent } from './categories/category-list/category-list.component';
import { FormsModule } from '@angular/forms';
import { SearchListComponent } from './search-list/search-list.component';
import { RatingModule } from 'ngx-bootstrap/rating';
import { ContactComponent } from './contact/contact.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    FooterComponent,
    RecipesMainComponent,
    RecipesListComponent,
    RecipeCardComponent,
    CategoryCardComponent,
    RecipeDetailsComponent,
    AboutComponent,
    HomeComponent,
    CategoryListComponent,
    SearchListComponent,
    ContactComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    FormsModule,
    PaginationModule.forRoot(),
    RatingModule.forRoot(),
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule { }
