import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent {
  searchQuery: string = ""
  constructor(private router: Router) { }

  submitSearch() {
    this.router.navigateByUrl("/search/" + this.searchQuery)
  }
}