import { Component, OnInit } from '@angular/core';
import { Category } from '../_models/category';
import { RecipesService } from '../_services/recipes.service';
import { CategoryService } from '../_services/category.service';
import { Recipe } from '../_models/recipe';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {
  categories: Category[]
  featured: Recipe[]
  latest: Recipe[]

  constructor(private recipeService: RecipesService, private categoryService: CategoryService) {

  }
  ngOnInit(): void {
    this.categoryService.getCategoriesPagedWithThumbnail(9).subscribe(x => this.categories = x)
    this.recipeService.getRecipesPaged(1, 3, "Updated.asc").subscribe(x => this.featured = x.list)
    this.recipeService.getRecipesPaged(1, 6, "Updated.desc").subscribe(x => this.latest = x.list)
  }

}
