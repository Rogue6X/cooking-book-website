import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent {
  model: any = {};
  showSuccessText = false;

  constructor(private http: HttpClient) {
  }

  submitForm() {
    console.log(this.model);
    this.http.post("https://mailthis.to/CookingBookEmail", this.model)
      .subscribe(resp => {
        console.log(resp)
        location.href = 'https://mailthis.to/confirm'
      }, error => {
        console.warn(error.responseText)
        console.log({ error })
      });
    this.showSuccessText = true;
  }
}
