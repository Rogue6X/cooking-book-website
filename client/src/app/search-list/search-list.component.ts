import { Component, OnInit, SimpleChanges } from '@angular/core';
import { Recipe } from '../_models/recipe';
import { Pagination } from '../_models/pagination';
import { RecipesService } from '../_services/recipes.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-search-list',
  templateUrl: './search-list.component.html',
  styleUrls: ['./search-list.component.css']
})
export class SearchListComponent implements OnInit {
  recipes: Recipe[];
  pageSize: number = 9;
  pageNumber: number = 1;
  pagination: Pagination;
  searchQuery: string | null;

  constructor(private recipeService: RecipesService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe((params) => {
      this.searchQuery = params.get('query');
      this.updatePage();
    });
  }

  updatePage() {
    if (!this.searchQuery || this.searchQuery == "")
      this.loadMembers()
    else
      this.loadMembersFiltered(this.searchQuery);
  }

  loadMembers() {
    this.recipeService.getRecipesPaged(this.pageNumber, this.pageSize)
      .subscribe(x => { this.recipes = x.list; this.pagination = x.page; })
  }

  loadMembersFiltered(searchQuery: string) {
    this.recipeService.getRecipesFromSearchString(searchQuery, this.pageNumber, this.pageSize)
      .subscribe(x => { this.recipes = x.list; this.pagination = x.page; })
  }

  pageChanged(event: any) {
    this.pageNumber = event.page;
    this.updatePage()
  }
}
