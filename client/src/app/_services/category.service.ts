import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Category } from '../_models/category';
import { CategoryPaged, PaginatedList } from '../_models/pagination';

@Injectable({
  providedIn: 'root',
})
export class CategoryService {
  baseUrl = environment.apiUrl;

  constructor(private http: HttpClient) { }

  getCategory(id: string) {
    return this.http.get<Category>(this.baseUrl + 'category/' + id);
  }

  getCategoriesPaged(
    pageNumber: number,
    pageSize: number,
    sortBy?: string
  ) {
    let params = new HttpParams();

    params = params.append('pageNumber', pageNumber);
    params = params.append('pageSize', pageSize);

    if (sortBy) params = params.append('sortBy', sortBy);

    return this.http.get<PaginatedList<Category>>(
      this.baseUrl + 'category/paged',
      {
        params,
      }
    );
  }

  getCategoriesPagedWithThumbnail(pageSize: number) {
    return this.http.get<Category[]>(
      this.baseUrl + 'category/'+pageSize+'/thumbnail',
    );
  }

  getCategoryWithPagedRecipes(urlName: string, pageNumber: number, pageSize: number) {
    return this.http.get<CategoryPaged>(this.baseUrl + 'category/recipes/' + urlName + '/paged', {
      params: {
        pageNumber: pageNumber, pageSize: pageSize,
      }
    });
  }

  getCategoriesRecipesCount(id: string) {
    return this.http.get<number>(this.baseUrl + 'category/' + id + '/recipes/count')
  }
}
