import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Review } from '../_models/review';

@Injectable({
    providedIn: 'root',
})
export class ReviewService {
    baseUrl = environment.apiUrl;

    constructor(private http: HttpClient) { }

    addNewReview(model: any) {
        return this.http.post<Review>(this.baseUrl + 'review', model)
    }
}
