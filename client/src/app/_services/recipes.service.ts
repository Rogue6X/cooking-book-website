import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Recipe } from '../_models/recipe';
import { PaginatedList } from '../_models/pagination';

@Injectable({
  providedIn: 'root',
})
export class RecipesService {
  baseUrl = environment.apiUrl;

  constructor(private http: HttpClient) { }

  getRecipe(id: string) {
    return this.http.get<Recipe>(this.baseUrl + 'recipe/' + id);
  }

  getRecipeByName(name: string) {
    return this.http.get<Recipe>(this.baseUrl + 'recipe/title/' + name);
  }

  getRecipesPaged(
    pageNumber: number,
    pageSize: number,
    sortBy?: string,
    filterBy?: string,
    filterValue?: string
  ) {
    let params = new HttpParams();

    params = params.append('pageNumber', pageNumber);
    params = params.append('pageSize', pageSize);

    if (sortBy) params = params.append('sortBy', sortBy);
    if (filterBy && filterValue) {
      params = params.append('filterBy', filterBy);
      params = params.append('filterValue', filterValue);
    }

    return this.http.get<PaginatedList<Recipe>>(this.baseUrl + 'recipe/paged', {
      params,
    });
  }

  getRecipesFromSearchString(title: string, pageNumber: number, pageSize: number) {
    return this.http.get<PaginatedList<Recipe>>(this.baseUrl + 'recipe/search/' + title, {
      params: {
        pageNumber: pageNumber,
        pageSize: pageSize
      }
    });
  }
}
