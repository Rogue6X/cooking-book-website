import { Recipe } from "./recipe";

export interface Category {
  id: string;
  name: string;
  urlName:string;
  recipes: Recipe[];
}
