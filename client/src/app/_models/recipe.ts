﻿import { Ingredient } from './ingredient';
import { Review } from './review';
import { Step } from './step';

export interface Recipe {
  id: string;
  title: string;
  urlTitle: string;
  originalLink: string;
  prepareTime:number
  cookTime: number;
  rating: number;
  description: string;
  yield: number;
  photoUrl: string;
  updated: Date;
  ingredients: Ingredient[]
  steps: Step[]
  reviews: Review[]
}
