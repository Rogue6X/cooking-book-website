﻿import { Product } from "./product";

export interface Ingredient {
  id: string;
  recipeId: string;
  productId: string;
  amount: number;
  unit: string;
  notes: string
  product: Product
}
