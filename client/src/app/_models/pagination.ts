import { Category } from "./category";

export interface Pagination {
  currentPage: number;
  totalPages: number;
  pageSize: number;
  totalCount: number;
}

export interface PaginatedList<T> {
  list: T[];
  page: Pagination;
}

export interface CategoryPaged {
  category: Category;
  page: Pagination;
}

