export class Review {
    id: string;
    recipeId: string;
    author: string;
    rating: number;
    text: string;
    created: Date;
}