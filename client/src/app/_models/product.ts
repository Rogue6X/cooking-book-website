﻿export interface Product {
  id: string;
  name: string;
  substitutes: string[];
}
